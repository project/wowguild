<?php
// $Id$
/**
 * @file
 * Contains administrative functions for wowtoon module.
 */

/**
 * Theme the wowtoon_settings form.  Convert weights into a draggable table.
 */
function theme_wowtoon_toon_settings_form($variables) {
  $form = $variables['form'];
  // Convert form items into a table.
  $table_rows = array();
  foreach (array('items', 'progression', 'achievement') as $key) {
    //theme_table does not use #weight, but we will sort manually.
    $row = array(
      'data' => array(),
      'class' => array('draggable'),
      '#weight' => $form['blocks']['weight-' . $key]['#default_value']
    );
    // Add Class so that "Show row weights" hide/shows
    $form['blocks']['weight-' . $key]['#attributes']['class'] = array('block-weights');
    
    // Move weight title to first row.
    $row['data'][] = $form['blocks']['weight-' . $key]['#title'];
    unset($form['blocks']['weight-' . $key]['#title']);
    // Render Display Form Item
    $row['data'][] = drupal_render($form['blocks']['display-' . $key]);
    // Render Weight Form Item
    $row['data'][] = drupal_render($form['blocks']['weight-' . $key]);
    $table_rows[] = $row;
  }
  $header = array(t("Block Order"), t("Display"), t("Order"));
  
  uasort($table_rows, 'element_sort');
  
  $form['blocks']['table'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $table_rows, 'attributes' => array('id' => 'block-order'))),
    '#weight' => -10
  );
  // Call add_tabledrag to add and setup the JS for us
  // The key thing here is the first param - the table ID
  // and the 4th param, the class of the form item which holds the weight
  drupal_add_tabledrag('block-order', 'order', 'sibling', 'block-weights');     

  // Remove to avoid recursion.
  unset($form['#theme']);
  return drupal_render($form);
}

/**
 * Allows the user to change the settings on a toon by toon basis.
 */
function wowtoon_toon_settings_form($form, &$form_state) {
  global $user;
  
  $toon = $form_state['build_info']['args'][0];
  
  $form['blocks'] = array(
    '#type' => 'fieldset',
    '#title' => 'Blocks Settings',
    '#collapsible' => true
  );
  
  $show_blocks = variable_get('wowtoon_blocks_settings_tid_' . $toon->tid, array(
    'progression' => array('display' => 1, 'weight' => 3, 'settings' => array()),
    'achievement' => array('display' => 1, 'weight' => 1, 'settings' => array(WOWTOON_FEED_ACHIEVEMENT => 1, WOWTOON_FEED_BOSS_KILL => 1, WOWTOON_FEED_CRITERIA => 0)),
    'items'       => array('display' => 1, 'weight' => 2, 'settings' => array()),
  ));
  
    
  $form['blocks']['display-items'] = array('#type' => 'checkbox',
    '#title' => t('Display Recent Items Block'),
    '#default_value' => $show_blocks['items']['display'],
  );
  $form['blocks']['display-progression'] = array('#type' => 'checkbox',
    '#title' => t('Display Progression Block'),
    '#default_value' => $show_blocks['progression']['display'],
    '#description' => t('This block only display for level %maxlevel characters', array('%maxlevel' => WOWTOON_MAXIMUM_TOON_LEVEL)),
    '#disabled' => $toon->level != WOWTOON_MAXIMUM_TOON_LEVEL,
  );
  $form['blocks']['display-achievement'] = array('#type' => 'checkbox',
    '#title' => t('Display Achievement Block'),
    '#default_value' => $show_blocks['achievement']['display'],
  );
  
  $form['blocks']['weight-items'] = array(
    '#title' => t('Recent Items'),
    '#type' => 'weight',
    '#default_value' => $show_blocks['items']['weight'],
  );
  $form['blocks']['weight-progression'] = array(
    '#title' => t('Progression'),
    '#type' => 'weight',
    '#default_value' => $show_blocks['progression']['weight'],
  );
  $form['blocks']['weight-achievement'] = array(
    '#title' => t('Achievements'),
    '#type' => 'weight',
    '#default_value' => $show_blocks['achievement']['weight'],
  );
  
  $form['blocks']['achievement_block'] = array(
    '#type' => 'fieldset',
    '#title' => 'Achievement Block',
    '#description' => t('Show the following on your achievement block.'),
    '#weight' => -7
  );
  $form['blocks']['achievement_block']['show_achievment_feed_' . WOWTOON_FEED_ACHIEVEMENT] = array('#type' => 'checkbox',
    '#title' => t('Completed Achievements'),
    '#default_value' => $show_blocks['achievement']['settings'][WOWTOON_FEED_ACHIEVEMENT],
    '#description' => 'Example: <em>Earned the achievement <a href="http://www.wowhead.com/?achievement=13">Level 80</a> for 10 points.</em>'
  );
  $form['blocks']['achievement_block']['show_achievment_feed_' . WOWTOON_FEED_CRITERIA] = array('#type' => 'checkbox',
    '#title' => t('Part of an Achievement (criteria)'),
    '#default_value' => $show_blocks['achievement']['settings'][WOWTOON_FEED_CRITERIA],
    '#description' => 'Example: <em>Completed step <strong>Left Ring</strong> of achievement <a href="http://www.wowhead.com/?achievement=557">Superior</a></em>.'
  );
  $form['blocks']['achievement_block']['show_achievment_feed_' . WOWTOON_FEED_BOSS_KILL] = array('#type' => 'checkbox',
    '#title' => t('Boss Kills'),
    '#default_value' => $show_blocks['achievement']['settings'][WOWTOON_FEED_BOSS_KILL],
    '#description' => t('Example: 6 Loken kills (Halls of Lightning)')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
    '#weight' => 100,
  );
  $form['#theme'] = 'wowtoon_toon_settings_form';
  return $form;
}
/**
 * Save toon settings data.
 */
function wowtoon_toon_settings_form_submit($form, &$form_state) {
  $toon = $form_state['build_info']['args'][0];
  $values = $form_state['values'];
  $show_blocks = variable_get('wowtoon_blocks_settings_tid_' . $toon->tid, array(
    'progression' => array('display' => 1, 'weight' => 3, 'settings' => array()),
    'achievement' => array('display' => 1, 'weight' => 1, 'settings' => array(WOWTOON_FEED_ACHIEVEMENT => 1, WOWTOON_FEED_BOSS_KILL => 1, WOWTOON_FEED_CRITERIA => 0)),
    'items'       => array('display' => 1, 'weight' => 2, 'settings' => array()),
  ));
  foreach (array('items', 'progression', 'achievement') as $key) {
    $show_blocks[$key]['display'] = $values['display-' . $key];
    $show_blocks[$key]['weight'] = $values['weight-' . $key];
  }
  $show_blocks['achievement']['settings'] = array(
    WOWTOON_FEED_ACHIEVEMENT =>  $values['show_achievment_feed_' . WOWTOON_FEED_ACHIEVEMENT],
    WOWTOON_FEED_BOSS_KILL =>  $values['show_achievment_feed_' . WOWTOON_FEED_BOSS_KILL],
    WOWTOON_FEED_CRITERIA =>  $values['show_achievment_feed_' . WOWTOON_FEED_CRITERIA],
  );
  variable_set('wowtoon_blocks_settings_tid_' . $toon->tid, $show_blocks);
}

/**
 * Edit My Toons
 */
function wowtoon_toons_admin($user) {
  
  /*
  $new_toon = array(
    'type' => 'wowtoon',
    'uid' => $user->uid
  );
  $build['toon_add'] = drupal_get_form('wowtoon_add_form', (object)$new_toon);  
  */
  
  $header = array(
    array('data' => t('Name'), 'field' => 't.name', 'sort' => 'asc'),
    array('data' => t('Realm'), 'field' => 't.realm'),
    'Actions'
  );
  
  $query = db_select('wowtoon_entity', 't')->extend('TableSort')->orderByHeader($header);
  $query->fields('t', array('tid', 'raceid', 'factionid', 'classid', 'class', 'race', 'gender', 'genderid', 'name', 'realm'));
  $query->join('wowtoon_owners', 'o', 'o.toon_key = t.toon_key');
  $query->fields('o', array('uid', 'status'));
  $query->condition('o.uid', $user->uid);
  $results = $query->execute();
  
  
  $rows = array();
  foreach ($results as $toon) {
    $row = array();
    $l = sprintf('<img src="http://us.battle.net/wow/static/images/icons/class/%d.gif" title="%s" width="18" height="18" />', $toon->classid, check_plain($toon->class)); 
    $l .= sprintf(' <img src="http://us.battle.net/wow/static/images/icons/race/%d-%d.gif" title="%s" width="18" height="18" /> ', $toon->raceid, $toon->genderid, check_plain($toon->race));
    $l .= l($toon->name, 'toon/' . $toon->tid, array('attributes' => array('class' => array('shadow', 'color-c' . $toon->classid))));
    $row[] = $l;
    $row[] = $toon->realm;
    
    $actions = array();
    $actions[] = l(t('remove'), 'user/' . $user->uid . '/toons/' . $toon->tid . '/confirm_remove');
    if ($toon->status != 1) {
      $actions[] = l(t('validate'), 'toon/' . $toon->tid . '/validate');
    }
    $row[] = implode(' ', $actions);
    $rows[] = $row;
  }
  
  $build['table'] = array(
    '#prefix' => '<h3>' . t('My World of Warcraft Characters') . '</h3>',
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows
  );
  return $build;
}

/**
 * Ask user to confirm toon deletion.
 */
function wowtoon_toons_admin_confirm_remove($form, &$form_state, $user, $tid) {
  $toon = wowtoon_load($tid);
  $form['toon'] = wowtoon_view($toon, 'teaser');
  $form['confirm']['#prefix'] = '<div>';
  $form['confirm']['#markup'] = t('Are you sure you want to remove %name from your account?', array('%name' => $toon->name));
  $form['confirm']['#suffix'] = '</div>';
  
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => 'Delete');
  $form['actions']['cancel']['#markup'] = l(t('Cancel'), 'user/' . $user->uid . '/toons');
  
  $form_state['#user'] = $user;
  $form_state['#toon'] = $toon;
  
  return $form;
}
function wowtoon_toons_admin_confirm_remove_submit($form, &$form_state) {
  $user = $form_state['#user'];
  $toon = $form_state['#toon'];
  
  wowtoon_delete($toon);
  $form_state['redirect'] = 'user/' . $user->uid . '/toons';
}
/**
 * System Settings Form
 */
function wowtoon_admin_form($form, &$form_state) {
  $form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => 'Cron Updates',
    '#collapsible' => TRUE,
    '#description' => t('Manage what is updated during cron runs.'),
  );
  $form['cron']['wowtoon_cronupdate'] = array('#type' => 'checkbox',
    '#title' => t('Update Characters during Cron Run?'),
    '#default_value' => variable_get('wowtoon_cronupdate', 1),
    '#description' => t('Checking this box will automatically fild the oldest characters in the database and refresh data from the armory every cron run.')
  );
  $form['cron']['wowtoon_updatedelta'] = array('#type' => 'textfield',
    '#title' => t('Update Threshhold'),
    '#default_value' => variable_get('wowtoon_updatedelta', 12),
    '#required' => TRUE,
    '#description' => t('Update characters that are more than XX hours old.')
  );
  $form['cron']['wowtoon_updatecount'] = array('#type' => 'textfield',
    '#title' => t('Update Count'),
    '#default_value' => variable_get('wowtoon_updatecount', 20),
    '#required' => TRUE,
    '#description' => t('How many characters should we update each run?  Be aware that if this number is set too high, Blizzard may temporally block your IP from the armory.')
  );
  $form['cron']['wowtoon_updateownedonly'] = array('#type' => 'checkbox',
    '#title' => t('Only update claimed toons?'),
    '#default_value' => variable_get('wowtoon_updateownedonly', 0),
    '#description' => t('Checking this box will disable the update of un-claimed toons.  (For example, if you browse a guild, the toons are loaded but not claimed)')
  );
  /** NOT IMPLEMENTED YET.  HANDLED BY WOWGUILD.MODULE
  $form['cron']['wowguild_cronupdate'] = array('#type' => 'checkbox',
    '#title' => t('Update Guilds during Cron Run?'),
    '#default_value' => variable_get('wowguild_cronupdate', 1),
    '#description' => t('Checking this box will automatically fild the oldest toons in the database and refresh data from the armory every cron run.')
  );
  $form['cron']['wowguild_updatedelta'] = array('#type' => 'textfield',
    '#title' => t('Update Threshhold'),
    '#default_value' => variable_get('wowguild_updatedelta', '24'),
    '#description' => t('Update Guilds that are more than XX hours old.')
  );
  $form['cron']['wowguild_updatecount'] = array('#type' => 'textfield',
    '#title' => t('Update Count'),
    '#default_value' => variable_get('wowguild_updatecount', 1),
    '#description' => t('How many toons should we update each run?  Be aware that if this number is set too high, Blizzard may temporally block your IP from the armory.')
  );
  **/
  $form['toon_display'] = array(
    '#type' => 'fieldset',
    '#title' => 'Character Display Settings',
    '#collapsible' => TRUE,
    '#description' => t('<em>Note</em>: These changes may not take effect until each character is updated.'),
  );
  $form['toon_display']['wowtoon_allow_item_details'] = array('#type' => 'checkbox',
    '#title' => t('Allow Detailed Items'),
    '#default_value' => variable_get('wowtoon_allow_item_details', 1),
    '#description' => t('Display item names, gems and enchants on the Character page?  If this is set, you may want to force the width of TABLE.summary-inventory to 740px. See %file to customize display.',
      array('%file' => drupal_get_path('module', 'wowtoon') . '/wowtoon.tpl.php')
    )
  );
  $form['toon_display']['wowtoon_cache_backgrounds'] = array('#type' => 'checkbox',
    '#title' => t('Save chararacter background with specs?'),
    '#default_value' => variable_get('wowtoon_cache_backgrounds', 0),
    '#description' => t('Save a copy of the character\'s backgroud image locally?  Checking this box will change the background when the user browses alternate specs.<br /><strong>Important Note</strong>: this function <em>may</em> increase your download bandwidth if you are running cron on many characters!'),
  );
  $form['toon_display']['wowtoon_cache_backgrounds_path'] = array('#type' => 'textfield',
    '#title' => t('Path to cache background images?'),
    '#default_value' => variable_get('wowtoon_cache_backgrounds_path', 'public://wowtoon_background_cache'),
    '#required' => TRUE,
    '#description' => t('Where should we store character background images?  This path can be anything Drupal can use as a file wrapper.')
  );
  $form['toon_display']['wowtoon_cache_backgrounds_url'] = array('#type' => 'textfield',
    '#title' => t('URL to background cache directory?'),
    '#default_value' => variable_get('wowtoon_cache_backgrounds_url', file_create_url('public://wowtoon_background_cache')),
    '#required' => TRUE,
    '#description' => t('What is the URL to access the cache directory.')
  );
  $form['default language'] = array(
    '#type' => 'fieldset',
    '#title' => 'Default language Settings',
    '#collapsible' => TRUE,
    '#description' => t('<em>Note</em>: Changes will not display until characters are updated.'),
  );
  $form['default language']['wowtoon_default_language'] = array(
    '#type' => 'radios',
    '#title' => t('wowguild selector language'),
    '#description' => t('French, German and Russian languages are only available on EU realms.'),
    '#options' => array(
      'www' => t('English'),
      'fr' => t('French'),
      'es' => t('Spanish'),
      'de' => t('German'),
      'ru' => t('Russian'),
    ),
    '#default_value' => variable_get('wowtoon_default_language', 'www'),
  );


  $form['guild_display'] = array(
    '#type' => 'fieldset',
    '#title' => 'Guild Display Settings',
    '#collapsible' => TRUE,
    '#description' => t('<em>Note</em>: These changes may not take effect until each guild is updated.'),
  );
  $form['guild_display']['wowtoon_html5_tabards'] = array('#type' => 'checkbox',
    '#title' => t('Use HTML5 to render tabards?'),
    '#default_value' => variable_get('wowtoon_html5_tabards', 1),
    '#description' => t('Blizzard uses HTML5 <em>canvas</em> element to render guild tabards.  Check this box if you would like to replicate this process.')
  );
  $form['guild_display']['wowtoon_html5_tabards_cache_path'] = array('#type' => 'textfield',
    '#title' => t('Directory to cache tabard files?'),
    '#default_value' => variable_get('wowtoon_html5_tabards_cache_path', 'public://tabardcache'),
    '#required' => TRUE,
    '#description' => t('HTML5 canvas drawing is limited to local files, so the image files needs to be cached on this webserver.  Enter the path you\'d like to automatically save files to.  This path can be anything Drupal can use as a file wrapper.')
  );
  $form['guild_display']['wowtoon_html5_tabards_cache_url'] = array('#type' => 'textfield',
    '#title' => t('URL to tabard cache directory?'),
    '#default_value' => variable_get('wowtoon_html5_tabards_cache_url', file_create_url('public://tabardcache')),
    '#required' => TRUE,
    '#description' => t('What is the URL to access the cache directory.')
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration')
  );
  return $form;
}
/**
 * Validate paths set by user.
 */
function wowtoon_admin_form_validate($form, &$form_state) {
  $values = $form_state['values']; 
}
/**
 * Save admin settings variables.
 */
function wowtoon_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  variable_set('wowtoon_cronupdate', $values['wowtoon_cronupdate']);
  variable_set('wowtoon_updatedelta', $values['wowtoon_updatedelta']);
  variable_set('wowtoon_updatecount', $values['wowtoon_updatecount']);
  variable_set('wowtoon_updateownedonly', $values['wowtoon_updateownedonly']);
  
  variable_set('wowtoon_allow_item_details', $values['wowtoon_allow_item_details']);
  variable_set('wowtoon_cache_backgrounds', $values['wowtoon_cache_backgrounds']);
  
  variable_set('wowtoon_cache_backgrounds_path', str_replace("\\", "/", $values['wowtoon_cache_backgrounds_path']));
  if (substr($values['wowtoon_cache_backgrounds_path'], 0, 9) == 'public://') {
    variable_set('wowtoon_cache_backgrounds_url', file_create_url(str_replace("\\", "/", $values['wowtoon_cache_backgrounds_path'])));
  }
  else {
    variable_set('wowtoon_cache_backgrounds_url', file_create_url(str_replace("\\", "/", $values['wowtoon_cache_backgrounds_url'])));
  }
  variable_set('wowtoon_html5_tabards', $values['wowtoon_html5_tabards']);
  variable_set('wowtoon_html5_tabards_cache_path', str_replace("\\", "/", $values['wowtoon_html5_tabards_cache_path']));
  if (substr($values['wowtoon_html5_tabards_cache_path'], 0, 9) == 'public://') {
    variable_set('wowtoon_html5_tabards_cache_url', file_create_url(str_replace("\\", "/", $values['wowtoon_html5_tabards_cache_path'])));
  }
  else {
    variable_set('wowtoon_html5_tabards_cache_url', file_create_url(str_replace("\\", "/", $values['wowtoon_html5_tabards_cache_url'])));
  }
  variable_set('wowtoon_default_language', $values['wowtoon_default_language']);
}