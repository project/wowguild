<?php
// $Id$
/**
 * @file
 * Render the achievements of a toon or guild.
 *
 * Available variables:
 * - $toon OR $guild => The parent object
 * - $type (achievement|items)
 * - $acheivements
 * Array
 * (
 *     [type] => integer type (see constants in wowtoon.module)
 *     [icon] => URL of the 18x18 icon 
 *     [icon_img] => full HTML icon
 *     [description] => HTML description.
 *     [datecompeted] => unix timestamp of date completed
 *     [datecompleted_ago] => Date completed formated as XX ago.
 * )
 *
 * @see template_preprocess_wowtoon_achievement_block()
 * @see template_preprocess()
 * @see template_process()
 */
if ($achievements):
?>
<div id="wowtoon-<?php echo $type; ?>-block">
<?php foreach ($achievements as $achievement) {?>
  <div class="clear-block achievement achievement-type-<?php echo $achievement->type;?>">
    <?php if (!empty($achievement->icon_img)):?><div class="icon"><?php echo $achievement->icon_img; ?></div><?php endif; ?>
    <div class="text"><?php echo $achievement->description; ?></div>
    <div class="time"><?php echo $achievement->datecompleted_ago; ?></div>
  </div>
<?php } ?>
</div>
<?php endif; ?>