<?php
// $Id$
/**
 * @file
 * Contains administrative functions for wowguild module.
 */


/**
 * Show the guild_application content type pages.
 * 
 * @param string (or FALSE) $show
 *   This parameter is passed from hook_menu, and denotes which field_data_application_status to show.
 *   
 * @return Array
 *   Renderable array of applications and tasks related to them.
 */
function wowguild_applications_view($show = FALSE) {
  drupal_add_js('var wowguildUpdateStatusUrl =' . drupal_json_encode(url('guild_admin/update_guild_application_status')) . ';', 'inline');
  drupal_add_js(drupal_get_path('module', 'wowguild') . '/js/wowguild.review-guild-applications.js');
  $header = array(array('data' => t('Name'), 'field' => 'n.title'));
  if ($show === FALSE) {
    $header[] = t('Status');
  }
  $header[] = array('data' => t('Date'), 'field' => 'n.created', 'sort' => 'desc'); 
  $header[] = t('Actions');
  
  
  $query = db_select('node', 'n')->extend('PagerDefault')->extend('TableSort')->orderByHeader($header);
  $query->addTag('node_access');
  $query->fields('n', array('nid', 'title', 'created'));
  $query->condition('n.type', 'guild_application');
  if ($show) {
    $query->join('field_data_application_status', 's', "s.entity_id = n.nid AND s.bundle = 'guild_application' AND deleted = 0");
    $query->condition('s.application_status_value', $show);
  }
  $query->condition('n.status', 1);
  $query->limit(20);
  $nids = $query->execute()->fetchCol();
  
  $applications = node_load_multiple($nids);
  
  // Get available application statuses.
  $field = field_info_field('application_status');
  $options = $field['settings']['allowed_values'];
  
  $rows = array();
  foreach ($applications as $app) {
    
    $row = array();
    if (!empty($app->postedby)) {
      $img = sprintf('<img src="%s" width="25" height="25" /> ', wowtoon_get_render($app->postedby, 'avatar'));
      $col = l($img, 'toon/' . $app->postedby->tid, array('html' => TRUE));
      $col .= l($app->title, 'node/' . $app->nid);
      $row[] = $col;
    }
    else {
      $row[] = l($app->title, 'node/' . $app->nid);
    }
    
    if ($show === FALSE) {
      // Display a fake 'pulldown' to change status that only displays IF user has javascript enabled.
      $col = '<div id="wowguild-change-application-status-nid' . $app->nid . '" class="wowguild-change-application-status" rel="' . $app->nid . '">' . $app->application_status[LANGUAGE_NONE][0]['value'];
      $col .= '<div id="wowguild-change-application-status-pulldown-nid' . $app->nid . '" class="wowguild-change-application-pulldown"><ul>';
      foreach ($options as $key => $value) {
        $col .= '<li>' . l($value, 'guild_admin/update_guild_application_status/' . $app->nid . '/' . $value,
        array('attributes' => array(
          'class' => array('wowguild-change-application-status-pulldown-status'),
          'rel' => $app->nid,
        ))
        ) . '</li>';
      }
      $col .= '</ul></div></div>';
      $row[] = $col; 
    }
    
    $row[] = format_date($app->created);
    $col = t('app:') . ' ' .
    l(t('view'), 'node/' . $app->nid) . ' | ' .
    l(t('edit'), 'node/' . $app->nid . '/edit', array('query' => array('destination' => 'guild_admin/guild_applications'))) . ' | ' .
    l(t('delete'), 'node/' . $app->nid . '/delete', array('query' => array('destination' => 'guild_admin/guild_applications'))) . '<br />';
    if (!empty($app->uid)) { 
      $col .= t('user:') . ' ' . l(t('edit'), 'user/' . $app->uid . '/edit');
      if (module_exists('mmo_notification') && !empty($app->postedby)) {
        $col .= ' | ' . l(t('mail'), 'inbox/compose', array('query' => array('sender_tid' => wowtoon_get_current_tid(), 'receiver_tid' => $app->postedby->tid)));
      }
    }
    else {
      // $col .= ' | ' . l(t('email'), 'mailto:' . check_plain($app->application_email[LANGUAGE_NONE][0]['value']));
    }
    $row[] = $col;
    $rows[] = $row;
  }
  
  $build = array();
  if (user_access('administer site configuration')) {
    $build['customize_app']['#markup'] = l(t('Customize guild application fields'), 'admin/structure/types/manage/guild-application/fields');
  }
  $build['apps'] = array(
    '#attributes' => array('id' => 'wowguild-review-applications-table'),
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows
  );
  $build['apps_pager'] = array('#theme' => 'pager');
  return $build;
  
}

/**
 * Allows admins to quickly change the application status of a guild_application.
 * 
 * @param $node
 *   The node being updated.
 *   
 * @param $status
 *   The new status for the passed node.
 *   
 * @return
 *   If $_GET['ajax'] is passed, then the output array is json_encoded and returned, otherwise, returns the user to the guild_admin/guild_applications page.
 */

function wowguild_update_application_status($node, $status) {
  if ($node->type == 'guild_application') {
    $node->application_status[$node->language][0]['value'] = check_plain($status);
    node_save($node);
    if (isset($_GET['ajax'])) {
      $results = array(
        'results' => 1,
        'nid' => $node->nid,
        'status' => check_plain($status)
      );
      drupal_json_output($results);
    }
    else {
      drupal_set_message(t('Updated @name application status to @status', array('@name' => $node->title, '@status' => $status)));
      drupal_goto('guild_admin/guild_applications');
    }
  }
}

/**
 * Shows the guild admins the guild roster and which users owns them for the guild.
 * 
 * @return
 *   Renderable array of guild members.
 */
function wowguild_manage_roster() {
  global $user;
  $guild = wowguild_load(wowguild_get_gid());
  
  // Create a user token to prevent hijacking of settings roles links.
  $user_token = rand(100,100000);
  variable_set('wowtoon_user_token_' . $user->uid, $user_token);
  
  $header = array(
    array('data' => t('Account'), 'field' => 'u.name'),
    array('data' => t('Toon(s)')),
    array('data' => t('Date Created'), 'field' => 'u.created', 'sort' => 'desc'),
    array('data' => t('Last Login'), 'field' => 'u.login'),
  );
  $roles = user_roles();
  $allowed_roles = array();
  foreach ($roles as $rid => $role) {
    if (variable_get('wowguild_roster_can_assign_role_rid' . $rid, $role=='guild member'?1:0)) {
      $allowed_roles[$rid] = $role;
      $header[] = t('is @role', array('@role' => $role));
    }
  }
  
  $query = db_select('wowtoon_entity', 't')->extend('PagerDefault')->extend('TableSort')->orderByHeader($header);
  $query->fields('t', array('tid', 'classid', 'guild_rank', 'name', 'realm'));
  $query->join('wowtoon_owners', 'o', 'o.tid = t.tid');
  $query->fields('o', array('uid', 'status'));
  $query->condition('t.gid', $guild->gid);
  $query->join('users', 'u', 'o.uid = u.uid');
  $query->addField('u', 'name', 'username');
  $query->addField('u', 'created');
  $query->addField('u', 'login');
  
  if (!empty($_GET['search'])) {
    $or = db_or();
    $or->condition('t.name', '%' . check_plain($_GET['search']) . '%', 'LIKE');
    $or->condition('u.name', '%' . check_plain($_GET['search']) . '%', 'LIKE');
    $query->condition($or);
  }
  
  $query->limit(20);
  $results = $query->execute()->fetchAll();
  
  $rows = array();
  $i = 0;
  while ($i < count($results)) {
    $toon = $results[$i];
    
    $row = array();
    $row[] = l($toon->username, 'user/' . $toon->uid);
    $user_toons = array();
    do {
      $this_toon = $results[$i];
      $user_toons[] = l($this_toon->name, 'toon/' . $this_toon->tid, array('attributes' => array('class' => array('shadow', 'color-c' . $this_toon->classid, $this_toon->status == 1?'validated-toon':'unvalidated-toon'))));
      $i++;
    } while ($i<count($results) && $results[$i]->uid == $this_toon->uid);
    $row[] = implode(' ', $user_toons); 
    if (REQUEST_TIME - $toon->created < 60*60*24*3) {
      $row[] = format_interval(REQUEST_TIME - $toon->created, 1);
    }
    else {
      $row[] = format_date($toon->created);
    }
    if (REQUEST_TIME - $toon->login < 60*60*24*3) {
      $row[] = format_interval(REQUEST_TIME - $toon->login, 1);
    }
    else {
      $row[] = format_date($toon->login);
    }
    
    $this_user = user_load($toon->uid);
    foreach ($allowed_roles as $rid => $role) {
      if (in_array($rid, array_keys($this_user->roles))) {
        $row[] = l('<img src="/' . drupal_get_path('module', 'wowguild') . '/images/checked.png" />', current_path() . '/set/' . $toon->uid . '/' . $rid, array(
          'html' => TRUE,
          'attributes' => array('ref' => $toon->uid . '~' . $rid,  'class' => array('checked_box')),
          'query' => array('token' => $user_token),
        ));
      }
      else {
        $row[] = l('<img src="/' . drupal_get_path('module', 'wowguild') . '/images/unchecked.png" />', current_path() . '/set/' . $toon->uid . '/' . $rid, array(
          'html' => TRUE,
          'attributes' => array('ref' => $toon->uid . '~' . $rid, 'class' => array('unchecked_box')),
          'query' => array('token' => $user_token),
        ));
      }
    }
    $rows[] = $row;
  }
  $build = array();
  
  $build['filters'] = drupal_get_form('wowguild_manage_roster_filter_form');
  $build['roster'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'manage-users-and-roster'),
    '#suffix' => '<span class="description">' . t('Note: Underlined toons have been validated by the user.') . '</span>',
  );
  $build['apps_pager'] = array('#theme' => 'pager');
  return $build;
}

/**
 * Defines a form that allows admins to filter the wowguild_manage_roster() page.
 * It is included on the above page. 
 */

function wowguild_manage_roster_filter_form($form) {
  $form['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search by name'),
    '#default_value' => isset($_GET['search'])?$_GET['search']:'',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('filter')
  );
  $form['#method'] = 'get';
  return $form;
}

/**
 * Helper function for a guild admin to set and remove user roles.
 */
function wowguild_manage_roster_set($uid, $rid, $ajax = FALSE) {
  global $user;
  
  // Verify user token for this request.
  $user_token = variable_get('wowtoon_user_token_' . $user->uid, 0);
  if ($_GET['token'] != $user_token) {
    die('Invalid Request');
  }
  $this_user = user_load($uid);
  
  $roles = user_roles();
  
  $img = '';
  $msg = '';
  
  if (array_key_exists($rid, $roles)) {
    // Idiot check, make sure we passing a valid role.
    if (array_key_exists($rid, $this_user->roles)) {
      db_query('DELETE FROM {users_roles} WHERE uid = :uid AND rid = :rid', array(':uid' => $this_user->uid, ':rid' => $rid));
      unset($this_user->roles[$rid]);
      $img = drupal_get_path('module', 'wowguild') . '/images/unchecked.png';
      $msg = t('Removed %role from @user.', array('%role' => $roles[$rid], '@user' => $this_user->name));
    }
    else {
      $this_user->roles[$rid] = $roles[$rid];
      db_query('INSERT INTO {users_roles} (uid, rid) VALUES (:uid, :rid)', array(':uid' => $this_user->uid, ':rid' => $rid));
      $img = drupal_get_path('module', 'wowguild') . '/images/checked.png';
      $msg = t('Added %role to @user.', array('%role' => $roles[$rid], '@user' => $this_user->name));
    }
  }
  
  if ($ajax !== FALSE) {
    print(drupal_json_encode(array(
      'img' => $img,
      'msg' => $msg
    )));
    die();
    return FALSE;
  }
  else {
    drupal_set_message($msg);
    drupal_goto('admin/config/wowguild/roster');
  }
}

/**
 * Themes the wowguild_admin_form form.  Converting the weight fields to a draggable table.
 * Based on: http://api.drupal.org/api/drupal/includes--common.inc/function/drupal_add_tabledrag/7
 * 
 * @param $variables
 *   Form fields.
 * 
 * @return
 *   The HTML rendered form.
 */
function theme_wowguild_admin_form($variables) {
  $form = $variables['form'];
  // Convert form items into a table.
  $table_rows = array();
  foreach (array('items', 'progression', 'achievement') as $key) {
    //theme_table does not use #weight, but we will sort manually.
    $row = array(
      'data' => array(),
      'class' => array('draggable'),
      '#weight' => $form['blocks']['weight-' . $key]['#default_value']
    );
    // Add Class so that "Show row weights" hide/shows
    $form['blocks']['weight-' . $key]['#attributes']['class'] = array('block-weights');
    
    // Move weight title to first row.
    $row['data'][] = $form['blocks']['weight-' . $key]['#title'];
    unset($form['blocks']['weight-' . $key]['#title']);
    // Render Display Form Item
    $row['data'][] = drupal_render($form['blocks']['display-' . $key]);
    // Render Weight Form Item
    $row['data'][] = drupal_render($form['blocks']['weight-' . $key]);
    $table_rows[] = $row;
  }
  $header = array(t("Block Order"), t("Display"), t("Order"));
  
  uasort($table_rows, 'element_sort');
  
  $form['blocks']['table'] = array(
    '#markup' => theme('table', array('header' => $header, 'rows' => $table_rows, 'attributes' => array('id' => 'block-order'))),
    '#weight' => -10
  );
  

  // Call add_tabledrag to add and setup the JS for us
  // The key thing here is the first param - the table ID
  // and the 4th param, the class of the form item which holds the weight
  drupal_add_tabledrag('block-order', 'order', 'sibling', 'block-weights');     

  // Remove to avoid recursion.
  unset($form['#theme']);
  return drupal_render($form);
}
  
/**
 * System Settings Form
 */
function wowguild_admin_form($form, &$form_state) {
  $form['guild'] = array(
    '#type' => 'fieldset',
    '#title' => 'Guild Settings',
    '#collapsible' => TRUE,
    '#weight' => -1,
  );
  
  $form['guild']['wowguild_guild_name'] = array('#type' => 'textfield',
    '#title' => t('Guild Name'),
    '#default_value' => variable_get('wowguild_guild_name', isset($toon->guild_name)?$toon->guild_name:''),
    '#required' => TRUE,
    '#description' => t('The exact name of the guild you are hosting.')
  );
  $form['guild']['wowguild_guild_realm'] = array('#type' => 'textfield',
    '#title' => t('Guild Realm'),
    '#default_value' => variable_get('wowguild_guild_realm', isset($toon->realm)?$toon->realm:''),
    '#required' => TRUE,
    '#description' => t('The realm that this guild is on.')
  );

  $form['guild']['wowguild_guild_limit_guild_links'] = array('#type' => 'checkbox',
    '#title' => t('Use Armory links for other guilds?'),
    '#default_value' => variable_get('wowguild_guild_limit_guild_links', 1),
    '#description' => t('If this is unchecked, then other guilds and toons will be loaded when a user clicked another guild.')
  );
  
  
  $gid = wowguild_get_gid();
  $show_blocks = variable_get('wowguild_blocks_settings_gid_' . $gid, array(
    'progression' => array('display' => 1, 'weight' => 3, 'settings' => array()),
    'achievement' => array('display' => 1, 'weight' => 1, 'settings' => array('achievement' => 1, 'boss_kill' => 1, 'criteria' => 0)),
    'items'       => array('display' => 1, 'weight' => 2, 'settings' => array()),
  ));
  
  
  $form['guild_ranks'] = array(
    '#type' => 'fieldset',
    '#title' => 'Guild Ranks',
    '#collapsible' => TRUE,
    '#weight' => 0,
  );
  
  $rank_names = variable_get('wowguild_guild_rank_names', array('Guild Master', 'Rank 1', 'Rank 2', 'Rank 3', 'Rank 4', 'Rank 5', 'Rank 6', 'Rank 7', 'Rank 8', 'Rank 9'));
  for ($i=0;$i<10;$i++) {
    $form['guild_ranks']['wowguild_guild_rank_' . $i] = array('#type' => 'textfield',
      '#title' => t('Guild Rank #' . $i . ' Name:'),
      '#default_value' => $rank_names[$i],
      '#required' => FALSE
    );
  }
  
  
  $form['blocks'] = array(
    '#type' => 'fieldset',
    '#title' => 'Blocks Settings',
    '#collapsible' => TRUE,
    '#weight' => 1,
  );
  
  $show_blocks = variable_get('wowtoon_blocks_settings_gid_' . $gid, array(
    'progression' => array('display' => 1, 'weight' => 3, 'settings' => array()),
    'achievement' => array('display' => 1, 'weight' => 1, 'settings' => array(
      WOWTOON_FEED_GUILD_ACHIEVEMENT => 1,
      WOWTOON_FEED_GUILD_PLAYER_ACHIEVEMENT => 1,
      WOWTOON_FEED_GUILD_LEVEL_UP => 1,
    )),
    'items'       => array('display' => 1, 'weight' => 2, 'settings' => array(
      WOWTOON_FEED_GUILD_ITEM_CRAFTED => 1,
      WOWTOON_FEED_GUILD_ITEM_LOOTED => 1,
      WOWTOON_FEED_GUILD_ITEM_PURCHASED => 1,
    )),
  ));
  
  $form['blocks']['display-items'] = array('#type' => 'checkbox',
    '#title' => t('Display Recent Items Block'),
    '#default_value' => $show_blocks['items']['display'],
  );
  $form['blocks']['display-progression'] = array('#type' => 'checkbox',
    '#title' => t('Display Progression Block'),
    '#default_value' => $show_blocks['progression']['display'],
  );
  $form['blocks']['display-achievement'] = array('#type' => 'checkbox',
    '#title' => t('Display Achievement Block'),
    '#default_value' => $show_blocks['achievement']['display'],
  );
  
  $form['blocks']['weight-items'] = array(
    '#title' => t('Recent Items'),
    '#type' => 'weight',
    '#default_value' => $show_blocks['items']['weight'],
  );
  $form['blocks']['weight-progression'] = array(
    '#title' => t('Progression'),
    '#type' => 'weight',
    '#default_value' => $show_blocks['progression']['weight'],
  );
  $form['blocks']['weight-achievement'] = array(
    '#title' => t('Achievements'),
    '#type' => 'weight',
    '#default_value' => $show_blocks['achievement']['weight'],
  );
  

  $form['blocks']['achievement_block'] = array(
    '#type' => 'fieldset',
    '#title' => 'achievement Block',
    '#description' => t('Show the following on your achievement block.'),
    '#weight' => -7
  );
  $form['blocks']['achievement_block']['show_achievment_feed_' . WOWTOON_FEED_GUILD_ACHIEVEMENT] = array('#type' => 'checkbox',
    '#title' => t('Guild Achievements'),
    '#default_value' => $show_blocks['achievement']['settings'][WOWTOON_FEED_GUILD_ACHIEVEMENT],
  );
  $form['blocks']['achievement_block']['show_achievment_feed_' . WOWTOON_FEED_GUILD_PLAYER_ACHIEVEMENT] = array('#type' => 'checkbox',
    '#title' => t('Guild Player Achievements'),
    '#default_value' => $show_blocks['achievement']['settings'][WOWTOON_FEED_GUILD_PLAYER_ACHIEVEMENT],
  );
  $form['blocks']['achievement_block']['show_achievment_feed_' . WOWTOON_FEED_GUILD_LEVEL_UP] = array('#type' => 'checkbox',
    '#title' => t('Guild Level Ups'),
    '#default_value' => $show_blocks['achievement']['settings'][WOWTOON_FEED_GUILD_LEVEL_UP],
  );
  
  $form['blocks']['items_block'] = array(
    '#type' => 'fieldset',
    '#title' => 'Items Block',
    '#description' => t('Show the following on your recent items block.'),
    '#weight' => -7
  );
  $form['blocks']['items_block']['show_item_feed_' . WOWTOON_FEED_GUILD_ITEM_CRAFTED] = array('#type' => 'checkbox',
    '#title' => t('Guild Item Crafted'),
    '#default_value' => $show_blocks['items']['settings'][WOWTOON_FEED_GUILD_ITEM_CRAFTED],
  );
  $form['blocks']['items_block']['show_item_feed_' . WOWTOON_FEED_GUILD_ITEM_LOOTED] = array('#type' => 'checkbox',
    '#title' => t('Guild Item Looted'),
    '#default_value' => $show_blocks['items']['settings'][WOWTOON_FEED_GUILD_ITEM_LOOTED],
  );
  $form['blocks']['items_block']['show_item_feed_' . WOWTOON_FEED_GUILD_ITEM_PURCHASED] = array('#type' => 'checkbox',
    '#title' => t('Guild Item Purchased'),
    '#default_value' => $show_blocks['items']['settings'][WOWTOON_FEED_GUILD_ITEM_PURCHASED],
  );
  
  $roles = user_roles();
  
  $form['guild_applications'] = array(
    '#type' => 'fieldset',
    '#title' => 'Guild Application',
    '#weight' => 2,
    '#description' => t('Hide Guild Application link for the following roles.  <em>Important</em>:  If anonymous access is allowed, be sure that the anonymous role has the "Guild Application: Create new content" !permission.',
      array('!permission' => l('permssion', 'admin/people/permissions'))
    )
  );
  foreach ($roles as $rid => $role) {
    $form['guild_applications']['wowguild_hide_apps_for_role_rid' . $rid] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($role),
      '#default_value' => variable_get('wowguild_hide_apps_for_role_rid' . $rid, 0)
    );
  }

  $form['guild_roster'] = array(
    '#type' => 'fieldset',
    '#title' => 'User and Guild Roster Page Settings',
    '#weight' => 2,
    '#description' => t('Users with "administer roster" permission can access !url and alter users roles.  What roles are these user allowed to set?<br/>Example, you might designate a role that can promote other users to guild member.',
      array('!url' => l('User and Guild Roster', 'admin/config/wowguild/roster'))
    )
  );
  
  foreach ($roles as $rid => $role) {
    $form['guild_roster']['wowguild_roster_can_assign_role_rid' . $rid] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($role),
      '#default_value' => variable_get('wowguild_roster_can_assign_role_rid' . $rid, $role=='guild member'?1:0)
    );
  }
  
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#weight' => 100
  );
  
  $form['#theme'] = 'wowguild_admin_form';
  return $form;
}
/**
 * Implementation of form_validate()
 * Validate that the guild exists in the database or armory.
 * 
 * If validation passes, $form_state['guild'] will be set to the guild entity.
 */
function wowguild_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $realm = _wowtoon_validate_realm($values['wowguild_guild_realm']);
  if ($realm === FALSE) {
    form_set_error('wowguild_guild_realm', t('%realm is an invalid realm.', array('%realm' => $values['wowguild_guild_realm'])));
  }
  else {
    // Check Database for Guild
    $guild = wowguild_load_by_realm_name($realm, $values['wowguild_guild_name']);
    if (empty($guild->gid)) {
      // Not in database, check Armory.
      module_load_include('inc', 'wowtoon', 'wowtoon.update');
      $results = wowguild_update_armory($realm, $values['wowguild_guild_name']);
      if (is_numeric($results)) {
        switch ($results) {
          case WOWTOON_UPDATE_CHARACTER_NOT_AVAILABLE:
            form_set_error('wowguild_guild_name', '%name could not be found on the realm %realm.  Verify that it exists on the armory.',
            array('%name' => $values['wowguild_guild_name'], '%realm' => $values['wowguild_guild_realm']));
            break;
          default:
          case WOWTOON_UPDATE_INVALID_PAGE:
            form_set_error('wowguild_guild_name', 'We received an invalid page from the armory.  It may be down, please try again later.');
        }
      }
      else {
        $form_state['guild'] = $results;
      }
    }
    else {
      $form_state['guild'] = $guild;
    }
  }
}
/**
 * Implementation of form_submit()
 * Save passed varaibles, and update the menu item $menu['guild/$guild->gid'] to the main menu.
 */
function wowguild_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $guild = $form_state['guild'];
  
  $last_gid = variable_get('wowguild_guild_gid', 0);
  variable_set('wowguild_guild_name', $guild->name);
  variable_set('wowguild_guild_realm', $guild->realm);
  variable_set('wowguild_guild_gid', $guild->gid);
  
  variable_set('wowguild_guild_limit_guild_links', $values['wowguild_guild_limit_guild_links']);
  
  module_load_include('inc', 'wowtoon', 'wowtoon.update');
  if ($guild->gid != $last_gid) {
    wowguild_update_feed($guild);
    wowguild_update_progression($guild);
  }
  
  // Saved menu link id.
  $mlid = variable_get('wowguild_guild_mlid', 0);
  if ($mlid) {
    $ml = menu_link_load($mlid);
    $ml['link_path'] = 'guild/' . $guild->gid;
  }
  else {
    $ml = array(
      'menu_name' => 'main-menu',
      'module' => 'wowguild',
      'link_path' => 'guild/' . $guild->gid,
      'router_path' => 'guild/%',
      'link_title' => t('Guild Roster'),
      'weight' => 5,
    );
  }
  menu_link_save($ml);
  variable_set('wowguild_guild_mlid', $ml['mlid']);
  
  
  // Save Ranks
  $rank_names = array();
  for ($i=0;$i<10;$i++) {
    $rank_names[$i] = $values['wowguild_guild_rank_' . $i];
  }
  variable_set('wowguild_guild_rank_names', $rank_names);
  
  
  $show_blocks = variable_get('wowtoon_blocks_settings_gid_' . $guild->gid, array(
    'progression' => array('display' => 1, 'weight' => 3, 'settings' => array()),
    'achievement' => array('display' => 1, 'weight' => 1, 'settings' => array(
      WOWTOON_FEED_GUILD_ACHIEVEMENT => 1,
      WOWTOON_FEED_GUILD_PLAYER_ACHIEVEMENT => 1,
      WOWTOON_FEED_GUILD_LEVEL_UP => 1,
    )),
    'items'       => array('display' => 1, 'weight' => 2, 'settings' => array(
      WOWTOON_FEED_GUILD_ITEM_CRAFTED => 1,
      WOWTOON_FEED_GUILD_ITEM_LOOTED => 1,
      WOWTOON_FEED_GUILD_ITEM_PURCHASED => 1,
    )),
  ));
  foreach (array('items', 'progression', 'achievement') as $key) {
    $show_blocks[$key]['display'] = $values['display-' . $key];
    $show_blocks[$key]['weight'] = $values['weight-' . $key];
  }
  $show_blocks['achievement']['settings'] = array(
    WOWTOON_FEED_GUILD_ACHIEVEMENT =>  $values['show_achievment_feed_' . WOWTOON_FEED_GUILD_ACHIEVEMENT],
    WOWTOON_FEED_GUILD_PLAYER_ACHIEVEMENT =>  $values['show_achievment_feed_' . WOWTOON_FEED_GUILD_PLAYER_ACHIEVEMENT],
    WOWTOON_FEED_GUILD_LEVEL_UP =>  $values['show_achievment_feed_' . WOWTOON_FEED_GUILD_LEVEL_UP],
  );
  $show_blocks['items']['settings'] = array(
    WOWTOON_FEED_GUILD_ITEM_CRAFTED =>  $values['show_item_feed_' . WOWTOON_FEED_GUILD_ITEM_CRAFTED],
    WOWTOON_FEED_GUILD_ITEM_LOOTED =>  $values['show_item_feed_' . WOWTOON_FEED_GUILD_ITEM_LOOTED],
    WOWTOON_FEED_GUILD_ITEM_PURCHASED =>  $values['show_item_feed_' . WOWTOON_FEED_GUILD_ITEM_PURCHASED],
  );
  
  variable_set('wowtoon_blocks_settings_gid_' . $guild->gid, $show_blocks);
  
  $roles = user_roles();
  foreach ($roles as $rid => $role) {
    variable_set('wowguild_hide_apps_for_role_rid' . $rid, $values['wowguild_hide_apps_for_role_rid' . $rid]);
  }
  foreach ($roles as $rid => $role) {
    variable_set('wowguild_roster_can_assign_role_rid' . $rid, $values['wowguild_roster_can_assign_role_rid' . $rid]);
  }
  
  drupal_set_message(t('Saved configuration for !guild.', array('!guild' => l($guild->name, 'guild/' . $guild->gid))));
}