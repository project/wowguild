(function ($) {
	
  Drupal.Wowguild = {};
  Drupal.Wowguild.autoAttach = function() {
    $('#wowguild-review-applications-table').find('.wowguild-change-application-status').mouseenter(function() {
      $('#wowguild-change-application-status-pulldown-nid' + $(this).attr('rel')).show();
    });
    $('#wowguild-review-applications-table').find('.wowguild-change-application-status').mouseleave(function() {
    	$('#wowguild-change-application-status-pulldown-nid' + $(this).attr('rel')).hide();
    });
    // This is the actual item the user clicks to change status.
    $('#wowguild-review-applications-table').find('.wowguild-change-application-status-pulldown-status').click(function() {
      $('#wowguild-change-application-status-nid' + $(this).attr('rel')).html('updating');
      $.ajax({
        url:$(this).attr('href'),
        data: {'ajax':1},
        dataType: 'json',
        success: function (data) {
          $('#wowguild-change-application-status-nid' + data.nid).html(data.status);
        },
        error: function() {
          $('#wowguild-change-application-status-nid' + $(this).attr('rel')).html('failed update');
        }
      });
      // Don't update page.
      return false;
    });
  };
  
  $(Drupal.Wowguild.autoAttach);
})(jQuery);