(function ($) {
  Drupal.Wowtoon = Drupal.Wowtoon || {};
  var wowtoon_popup_cache = new Array();
  var wowtoon_loading_popup = false;
  var wowtoon_popup_tid = 0;
  
  Drupal.Wowtoon.autoAttach = function() {
	  // Attach realm lookup to edit realm fields.
	  $("#edit-realm").autocomplete({
	  	source: all_realms
	  });
	  
      $("#select-toon-block-char-arrow").mouseenter(
    	function() { $("#select-toon-block-pulldown").show(); }
      );
      $("#select-toon-block-pulldown").mouseleave(
        function() { $("#select-toon-block-pulldown").hide(); }
      );
      // JavaScript is enabled.  Hide select.
      $("#wowtoon-set-active-toon-form").css('display','none');
      $("#select-toon-block-char-arrow").css('display','inline');
      
      // Make the replacement select DIV trigger the hidden select form.
      $("#block-wowtoon-wowtoon-select-toon").find(".select-toon-block-select-item").click(function() {
        $("#wowtoon-set-active-toon-form SELECT").val($(this).attr('rel'));
        $("#wowtoon-set-active-toon-form").submit();
        return false;
      });
      
      // Add popup DIV
      $("BODY").append('<div id="wowtoon-popup"></div>');
      
      // Loop over links and find /toon/%toon links.  TODO: Adjust for q=/toon/ links.
      $("DIV.content").find("a").each(function (index) {
    	  // Don't attach to links with the class "np"
    	  if($(this).attr('href') && ($(this).attr('href').substr(0,6) == '/toon/' || $(this).attr('href').substr(0,9) == '/?q=toon/') && !$(this).hasClass('np')) {
    		  var tid_string = $(this).attr('href').split('/');
    		  var tid = parseInt(tid_string.pop());
    		  if(tid) {
    			// Remove native browser popup
    			$(this).attr('title','');
    			$(this).mousemove(function(ev) {
    				if(wowtoon_loading_popup == false) {
    					if(wowtoon_popup_tid != tid) {
    						// We are looking at a new toon.
		    				var found = -999;
		    				for(var i=0;i<wowtoon_popup_cache.length;i++) {
		    					if(wowtoon_popup_cache[i][0] == tid) {
		    						found = i;
		    					}
		    				}
		    				if(found != -999) {
		    					$("#wowtoon-popup").html(wowtoon_popup_cache[found][1]); // 'cache hit ' + 
		      				    wowtoon_popup_tid = tid;
		    				} else {
		    				  $("#wowtoon-popup").html('<div class="summary-popup"><div class="popup-text-wrapper">loading...</div></div>');
		    				  $("#wowtoon-popup").css('display','block');
		    				  var popup_url = $(this).attr('href') + '/popup';
		    				  $("#wowtoon-popup").load(popup_url, function(txt) {
		      				    wowtoon_popup_cache.push(new Array(tid,txt));
		      				    wowtoon_loading_popup = false;
		      				    wowtoon_popup_tid = tid;
		      		          });
		    				  wowtoon_loading_popup = true;
		    				}
	    				}
	    				
    				}
    				//$("#wowtoon-popup").position({my: "left top", at: "top right", of: $(this), offset: "5 0", collision: "flip"});
    				$("#wowtoon-popup").css('display','block');
    				$("#wowtoon-popup").position({my: "left top", at: "top right", of: ev, offset: "10 0", collision: "flip"});
    			});
    			$(this).mouseleave(function (ev) {
    				$("#wowtoon-popup").css('display','none');
    			});
    		  }
    	  }
      });
    };
    
    // Fake Blizzard's zeroFill function.


  $(Drupal.Wowtoon.autoAttach);
})(jQuery);