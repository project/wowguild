<?php
/**
 * @file
 * Called when a node has an attached 'posted_by' toon attached to it.
 * Displays 
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $toon: The toon begin displayed
 * - $guild_name: The sanitized guild name.
 * - $guild_url: URL if the toon's guild.
 * - $avatar: Avatar image of the toon.
 * 
 * @see template_preprocess_wowtoon()
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

// We are only interested in a couple of the toon fields.  Hide the rest.
hide($content['wowtoon_name']);
hide($content['wowtoon_realm']);
hide($content['wowtoon_fullname']);
hide($content['wowtoon_guild_name']);
hide($content['wowtoon_avg_ilevel']);
hide($content['wowtoon_avg_ilevel_best']);
hide($content['wowtoon_level']);
hide($content['wowtoon_battlegroup']);
hide($content['wowtoon_achievement_points']);

?>

<div class="summary-postedby <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <a href="/toon/<?php echo $toon->tid; ?>"><img src="<?php echo $avatar; ?>" class="avatar" /></a>
  <a href="/toon/<?php echo $toon->tid; ?>"><?php echo render($content['wowtoon_fullname']); ?></a>
  <?php if ($guild_name) {
    echo '<div class="guild">';
    if ($guild_url) {
      echo l($guild_name, $guild_url);
    } else {
      echo $guild_name;
    }
    echo "</div>";
  }
  ?>
  
  <div class="level-class-race color-c<?php echo (integer)$toon->classid; ?>">
    <strong><?php echo $content['wowtoon_level'][0]['#markup']; ?></strong>
    <?php echo check_plain($toon->race); ?>
    <?php echo check_plain($toon->class); ?>
  </div>
</div>









