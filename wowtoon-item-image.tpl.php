<?php
/**
 * @file
 * Renders the images of an item within a character display.
 *
 * Available variables:
 * - $item: item array.
 * - $gems: array of html image and links
 * - $icon: http link to icon
 * - $url: link to item on wowhead
 * - $rel: additional info (gems and enchants send to wowhead to append
 * 
 * Other variables:
 * - $slot: Highlight the abs(slot) slot if set.  Negitive numbers add the class unequip-me.  Positive numbers add the class 'equip-me'
 * 
 * @see template_preprocess_wowtoon_item_image()
 * 
 * @see wowtoon_validate_toon_form()
 */
?>


<?php if (isset($item['name'])):?>
<div class="slot slot-<?php echo $item['slot']; ?> item-quality-<?php echo $item['quality']; if ((integer)$item['data-id'] == abs($slot)) { echo $slot<0?' unequip-me':' equip-me'; } ?>" style="background:url('<?php echo $icon; ?>') no-repeat scroll 0 0 transparent; width:49px; height: 49px;">
</div>
<?php else: ?>
<div class="slot slot-<?php echo $item['slot']; if ((integer)$item['data-id'] == abs($slot)) { echo $slot<0?' unequip-me':' equip-me'; } ?>"><a href="javascript:;" class="empty"><span class="frame"></span></a></div>
<?php endif; ?>
