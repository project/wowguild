<?php
// $Id$

/**
 * @file
 * This will display when a user is required to validate a toon. 
 *
 * Available variables:
 *   - $toon: An array of profile items. Use render() to print them.
 *   - $slot: The slot to be removed or equiped.
 *
 * @see wowtoon_validate_toon_form()
 * @see wowtoon-item-image.tpl.php
 *   Where the html is handled for an item.
 */

$set = current($toon->sets[$toon->active_set_id]);

?>

<div id="wowtoon-validation-instructions" class="node-sticky">
<?php 
echo '<p>' . t("The character you have selected has also been selected by another user!  But don't worry, we just need to do a little extra validation.") . '</p>';
echo '<p>' . t("In order to verify that you own this character, please log into the character and ");
if ($slot > 0) {
  echo t('equip an item');
} else {
  echo t('unequip the item');
}
echo t(" in your %slot slot.", array('%slot' => $slot_lookup[abs($slot)])) . '</p>';
echo '<p>' . t("Once you have finished and logged out of this character, click 'Validate Character' below.") . '</p>';
?>
</div>
<table border="0" class="summary-inventory" width="740" >
<tr>
  <td class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[0], 'slot' => $slot)); ?></td>
  <td class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[9], 'slot' => $slot)); ?></td>
</tr>
<tr>
  <td class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[1], 'slot' => $slot)); ?></td>
  <td class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[5], 'slot' => $slot)); ?></td>
</tr>
<tr>
  <td class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[2], 'slot' => $slot)); ?></td>
  <td class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[6], 'slot' => $slot)); ?></td>
</tr>
<tr>
  <td class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[14], 'slot' => $slot)); ?></td>
  <td class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[7], 'slot' => $slot)); ?></td>
</tr>
<tr>
  <td class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[4], 'slot' => $slot)); ?></td>
  <td class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[10], 'slot' => $slot)); ?></td>
</tr>
<tr>
  <td class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[3], 'slot' => $slot)); ?></td>
  <td class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[11], 'slot' => $slot)); ?></td>
</tr>
<tr>
  <td class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[18], 'slot' => $slot)); ?></td>
  <td class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[12], 'slot' => $slot)); ?></td>
</tr>
<tr>
  <td class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[8], 'slot' => $slot)); ?></td>
  <td class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[13], 'slot' => $slot)); ?></td>
</tr>
</table>
<table border="0" class="summary-inventory" width="740">
<tr>
  <td width="33%" class='right-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[15], 'slot' => $slot)); ?></td>
  <td width="33%" class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[16], 'slot' => $slot)); ?></td>
  <td width="33%" class='left-item'><?php echo theme('wowtoon_item_image',array('item' => $set->items[17], 'slot' => $slot)); ?></td>
</tr>
</table>
