<?php
// $Id$
/**
 * @file
 * Defines default views for wowtoon module.
 * 
 */
/**
 * Implement hook_views_default_views().
 */
function wowguild_views_default_views() {
$view = new view;
$view->name = 'guild_pages';
$view->description = '';
$view->tag = '';
$view->base_table = 'wowtoon_entity';
$view->human_name = 'Guild Pages';
$view->core = 7;
$view->api_version = '3.0-alpha1';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'icon' => 'description',
  'description' => 'description',
  'datecompleted' => 'datecompleted',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'icon' => array(
    'align' => '',
    'separator' => '',
  ),
  'description' => array(
    'align' => '',
    'separator' => ' ',
  ),
  'datecompleted' => array(
    'sortable' => 0,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['order'] = 'desc';
$handler->display->display_options['style_options']['empty_table'] = 0;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'text_area';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = 'There are no achievements to list.';
/* Field: achievement: icon */
$handler->display->display_options['fields']['icon']['id'] = 'icon';
$handler->display->display_options['fields']['icon']['table'] = 'wowtoon_saved_achievements';
$handler->display->display_options['fields']['icon']['field'] = 'icon';
$handler->display->display_options['fields']['icon']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['icon']['alter']['text'] = '<img src="[icon]" >';
$handler->display->display_options['fields']['icon']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['icon']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['icon']['alter']['trim'] = 0;
$handler->display->display_options['fields']['icon']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['icon']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['icon']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['icon']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['icon']['alter']['html'] = 0;
$handler->display->display_options['fields']['icon']['element_label_colon'] = 1;
$handler->display->display_options['fields']['icon']['element_default_classes'] = 1;
$handler->display->display_options['fields']['icon']['hide_empty'] = 0;
$handler->display->display_options['fields']['icon']['empty_zero'] = 0;
$handler->display->display_options['fields']['icon']['icon_size'] = '18';
/* Field: achievement: Description */
$handler->display->display_options['fields']['description']['id'] = 'description';
$handler->display->display_options['fields']['description']['table'] = 'wowtoon_saved_achievements';
$handler->display->display_options['fields']['description']['field'] = 'description';
$handler->display->display_options['fields']['description']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['description']['alter']['text'] = '[description]';
$handler->display->display_options['fields']['description']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['description']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['description']['alter']['trim'] = 0;
$handler->display->display_options['fields']['description']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['description']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['description']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['description']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['description']['alter']['preserve_tags'] = '<a>';
$handler->display->display_options['fields']['description']['alter']['html'] = 0;
$handler->display->display_options['fields']['description']['element_label_colon'] = 1;
$handler->display->display_options['fields']['description']['element_default_classes'] = 1;
$handler->display->display_options['fields']['description']['hide_empty'] = 0;
$handler->display->display_options['fields']['description']['empty_zero'] = 0;
/* Field: achievement: Completed */
$handler->display->display_options['fields']['datecompleted']['id'] = 'datecompleted';
$handler->display->display_options['fields']['datecompleted']['table'] = 'wowtoon_saved_achievements';
$handler->display->display_options['fields']['datecompleted']['field'] = 'datecompleted';
$handler->display->display_options['fields']['datecompleted']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['datecompleted']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['datecompleted']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['datecompleted']['alter']['external'] = 0;
$handler->display->display_options['fields']['datecompleted']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['datecompleted']['alter']['trim'] = 0;
$handler->display->display_options['fields']['datecompleted']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['datecompleted']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['datecompleted']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['datecompleted']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['datecompleted']['alter']['html'] = 0;
$handler->display->display_options['fields']['datecompleted']['element_label_colon'] = 1;
$handler->display->display_options['fields']['datecompleted']['element_default_classes'] = 1;
$handler->display->display_options['fields']['datecompleted']['hide_empty'] = 0;
$handler->display->display_options['fields']['datecompleted']['empty_zero'] = 0;
$handler->display->display_options['fields']['datecompleted']['use_interval'] = '2';
/* Sort criterion: achievement: Completed */
$handler->display->display_options['sorts']['datecompleted']['id'] = 'datecompleted';
$handler->display->display_options['sorts']['datecompleted']['table'] = 'wowtoon_saved_achievements';
$handler->display->display_options['sorts']['datecompleted']['field'] = 'datecompleted';
$handler->display->display_options['sorts']['datecompleted']['order'] = 'DESC';
/* Contextual filter: achievement: keyid */
$handler->display->display_options['arguments']['keyid']['id'] = 'keyid';
$handler->display->display_options['arguments']['keyid']['table'] = 'wowtoon_saved_achievements';
$handler->display->display_options['arguments']['keyid']['field'] = 'keyid';
$handler->display->display_options['arguments']['keyid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['keyid']['summary']['format'] = 'default_summary';

/* Display: Guild Achievements */
$handler = $view->new_display('page', 'Guild Achievements', 'guild_achievements');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: achievement: keyid */
$handler->display->display_options['arguments']['keyid']['id'] = 'keyid';
$handler->display->display_options['arguments']['keyid']['table'] = 'wowtoon_saved_achievements';
$handler->display->display_options['arguments']['keyid']['field'] = 'keyid';
$handler->display->display_options['arguments']['keyid']['default_action'] = 'default';
$handler->display->display_options['arguments']['keyid']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['keyid']['default_argument_options']['code'] = 'return wowguild_get_gid();';
$handler->display->display_options['arguments']['keyid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['keyid']['break_phrase'] = 0;
$handler->display->display_options['arguments']['keyid']['not'] = 0;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: achievement: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'wowtoon_saved_achievements';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['operator'] = '>=';
$handler->display->display_options['filters']['type']['value']['value'] = '100';
$handler->display->display_options['path'] = 'guild_achievements';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Guild Achievements';
$handler->display->display_options['menu']['weight'] = '4';
$handler->display->display_options['menu']['name'] = 'main-menu';
$translatables['guild_pages'] = array(
  t('Defaults'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('text_area'),
  t('There are no achievements to list.'),
  t('icon'),
  t('<img src="[icon]" >'),
  t('Description'),
  t('[description]'),
  t('Completed'),
  t('All'),
  t('Guild Achievements'),
);
  
  
  $views[$view->name] = $view;

  return $views;
}


  