<?php
// $Id$

/**
 * @file
 * Render the achievements of a toon or guild.
 * Note: this code is re-used for Recent Items block.
 *
 * Available variables:
 * - $toon OR $guild => The parent object
 * - $progression
 *     Array of stdClass Object's
 *       (
 *           [name] => Baradin Hold - Instance Name
 *           [difficulity] => normal - (normal|heroic)
 *           [difficulity_text] => normal - translated text.
 *           [killed] => Array() - Array of names of bosses killed.
 *           [count] => 1 - Total number of bosses in encounter
 *           [kills] => 0 - Number killed
 *           [bosses] => Array - Array of bosses and number of kills.
 *               (
 *                   [0] => stdClass Object
 *                       (
 *                           [name] => Argaloth
 *                           [nKills] => 0
 *                       )
 *
 *               )
 *
 *           [class] => notstarted - (complete|inprogress|notstarted)
 *       )
 *
 * @see template_preprocess_wowtoon_progression_block()
 * @see template_preprocess()
 * @see template_process()
 */
if ($progression):
?>
<div id="wowtoon-progression-block">
<?php
if (!empty($toon->tid)) {
  foreach ($progression as $instance) {
    if ($instance->difficulity == 'normal') { echo "<h4>" . $instance->name . "</h4>\n"; }
    echo sprintf("<div class='%s'>%s [%d/%d]</div>", $instance->class, $instance->difficulity_text, $instance->kills, $instance->count);
  }
}
if (!empty($guild->gid)) {
  foreach ($progression as $instance) {
    echo sprintf("<div class='%s'>%s [%d/%d]</div>", $instance->class, $instance->name, $instance->kills, $instance->count);
  }
}

?>
</div>
<?php endif; ?>