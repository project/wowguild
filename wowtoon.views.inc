<?php
// $Id$
/**
 * @file
 * Provide views data and handlers for wowtoon.module
 */

/**
 * Implements hook_views_data()
 */
function wowtoon_views_data() {
  // ----------------------------------------------------------------
  // users table

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['wowtoon_entity']['table']['group']  = t('WoWToon');
  $data['wowtoon_entity']['table']['base'] = array(
    'field' => 'tid',
    'title' => t('Toon'),
    'help' => t('World of Warcraft Characters.'),
    'access query tag' => 'user_access',
  );
  $data['wowguild_entity']['table']['group']  = t('WoWGuild');
  $data['wowguild_entity']['table']['base'] = array(
    'field' => 'gid',
    'title' => t('Guild'),
    'help' => t('World of Warcraft Guild.'),
    'access query tag' => 'user_access',
  );
  $data['wowguild_entity']['table']['join'] = array(
    'wowtoon_entity' => array(
      'left_field' => 'gid',
      'field' => 'gid',
    )
  );  
  $data['wowtoon_owners']['table']['join'] = array(
    'wowtoon_entity' => array(
      'left_field' => 'tid',
      'field' => 'tid',
    )
  );
  $data['users']['table']['join'] = array(
    'wowtoon_entity' => array(
      'left_table' => 'wowtoon_owners',
      'left_field' => 'uid',
      'field' => 'uid',
    )
  );
  
  $data['wowtoon_saved_achievements']['table']['group']  = t('achievement');
  $data['wowtoon_saved_achievements']['table']['base'] = array(
    'field' => 'achid',
    'title' => t('WoW achievements'),
    'help' => t('World of Warcraft achievements [toon and guild].'),
    'access query tag' => 'user_access',
  );
  $data['wowtoon_saved_achievements']['table']['join'] = array(
    'wowtoon_entity' => array(
      'left_field' => 'tid',
      'field' => 'keyid',
      /*'extra' => array(
        'operator' => '<',
        'field' => 'type',
        'value' => WOWTOON_FEED_GUILD_achievement,
      )*/
  ),
    'wowguild_entity' => array(
      'left_field' => 'gid',
      'field' => 'keyid',
      /*'extra' => array(
        'operator' => '>=',
        'field' => 'type',
        'value' => WOWTOON_FEED_GUILD_achievement,
      )*/
    ),
  );
  
  /** Achievements **/
  $data['wowtoon_saved_achievements']['datecompleted'] = array(
    'title' => t('Completed'),
    'help' => t('Date Completed'),
    'field' => array(
      'field' => 'datecompleted',
      'real_field' => 'datecompleted',
      'handler' => 'wowtoon_handler_field_saved_achievements',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  $data['wowtoon_saved_achievements']['keyid'] = array(
    'title' => t('keyid'),
    'help' => t('entity id (wowtoon.tid / wowguild/gid) this achievement is associated with.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'keyid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  $data['wowtoon_saved_achievements']['type'] = array(
    'title' => t('Type'),
    'help' => t('achievement Type'),
    'field' => array(
      'field' => 'type',
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'type',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['wowtoon_saved_achievements']['icon'] = array(
    'title' => t('icon'),
    'help' => t('achievement Icon'),
    'field' => array(
      'field' => 'icon',
      'handler' => 'wowtoon_handler_field_saved_achievements',
      'click sortable' => FALSE,
    ),
  );
  $data['wowtoon_saved_achievements']['description'] = array(
    'title' => t('Description'),
    'help' => t('Achievement Description'),
    'field' => array(
      'field' => 'description',
      'handler' => 'wowtoon_handler_field_saved_achievements',
      'click sortable' => FALSE,
    ),
  );
  
  /** WoW Toons **/
  $data['wowtoon_entity']['tid'] = array(
    'title' => t('Tid'),
    'help' => t('The toon ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'tid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['wowtoon_entity']['name'] = array(
    'title' => t('Name'),
    'help' => t('Character Name'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_text',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_text',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  $data['wowtoon_entity']['realm'] = array(
    'title' => t('Realm'),
    'help' => t('Character Realm'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_text',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_text',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  $data['wowtoon_entity']['level'] = array(
    'title' => t('Level'),
    'help' => t('Character Level'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  $data['wowtoon_entity']['guild_name'] = array(
    'title' => t('Guild Name'),
    'help' => t('Name of Guild'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_text',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_text',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );    
  
  $data['wowtoon_entity']['guild_rank'] = array(
    'title' => t('Guild Rank'),
    'help' => t('Characters rank in the guild'),
    'field' => array(
      'field' => 'guild_rank',
      'handler' => 'wowtoon_handler_field_wowtoon_entity',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );    
  $data['wowtoon_entity']['guild_weekly'] = array(
    'title' => t('Weekly Points'),
    'help' => t('Points earned for the guild this week'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );    
  $data['wowtoon_entity']['guild_lifetime'] = array(
    'title' => t('Lifetime Points'),
    'help' => t('Points earned for the guild'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );    
  $data['wowtoon_entity']['achievement_points'] = array(
    'title' => t('Achievement Points'),
    'help' => t('Character Achievement Points'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );    
  $data['wowtoon_entity']['avg_ilevel'] = array(
    'title' => t('iLevel'),
    'help' => t('Item level of items equiped'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );    
  $data['wowtoon_entity']['avg_ilevel_best'] = array(
    'title' => t('iLevel Best'),
    'help' => t('Item level os all items owned by character'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'name',
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );    
  
  
  
  $data['wowtoon_entity']['gid'] = array(
    'title' => t('Gid'),
    'help' => t('The guild ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'tid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // class
  // Class can be displayed as text or an image.
  $data['wowtoon_entity']['class'] = array(
    'title' => t('Class'),
    'help' => t("Displays Character's Class."),
    'field' => array(
      'field' => 'class',
      'handler' => 'wowtoon_handler_field_wowtoon_entity',
    ),
  );
  // race
  // Race can be displayed as text or an image.
  $data['wowtoon_entity']['race'] = array(
    'title' => t('Race'),
    'help' => t("Displays Character's Race."),
    'field' => array(
      'field' => 'race',
      'handler' => 'wowtoon_handler_field_wowtoon_entity',
    ),
  );
  
  /** WoWGuilds **/
  $data['wowguild_entity']['gid'] = array(
    'title' => t('Gid'),
    'help' => t('The Guild ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'gid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  return $data;
}


/**
* Implementation of hook_views_handlers()
*
* hook_views_handlers tells the views module how to present fields within the UI and also how to
* display fields within a view itself. You will find a file corresponding to each parent item
* in the module directory.
* 
* format: [module]_handler_[type]_[tablename]_[fieldname]
* 
* This function does not seem to be used in Drupal 7.
*/

function wowtoon_views_handlers() {
  return array(
  'info' => array(
    'path' => drupal_get_path('module', 'wowtoon') . '/includes/views/',
    ),
    'handlers' => array(
      'views_handler_wowtoon_entity_tid' => array(
         'parent' => 'views_handler_field',
       ),
      'views_handler_argument_wowtoon_entity_tid' => array(
         'parent' => 'views_handler_argument_numeric',
       ),
       'wowtoon_handler_field_wowtoon_entity_class_img' => array(
         'parent' => 'views_handler_field',
         'file' => 'wowtoon_handler_field_wowtoon_entity_class_img.inc'
       ),
    ),
  );
}