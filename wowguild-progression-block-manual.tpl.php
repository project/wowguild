<?php
// $Id$

/**
 * @file
 * Render the achievements of a guild from data manually entred in the block configuration.
 *
 * Available variables:
 * - $raids
 * - $progression
 * @see template_preprocess()
 * @see template_process()
 */
?>
<div id="wowguild-progression-manual-block">
<?php
foreach ($progression as $raid) {
  $rows = array();
  $row = array();
  $head = array();
  if ($raid->enable) {
    if ($raid->show_numeric) {
      $head = array(array('data' => theme('image', array('path' => $raid->icon)), 'class' => array('wowguild-progression-manual-block-icon', 'wowguild-progression-manual-block-raid')), array('data' => t($raid->name), 'colspan' => 1, 'class' => 'wowguild-progression-manual-block-raid'), array('data' => $raid->numeric_summary, 'colspan' => 2, 'class' => array('wowguild-progression-manual-block-icon-numericsummary', 'wowguild-progression-manual-block-raid')));
    }
    else {
      $head = array(array('data' => theme('image', array('path' => $raid->icon)), 'class' => array('wowguild-progression-manual-block-icon', 'wowguild-progression-manual-block-raid')), array('data' => t($raid->name), 'colspan' => 3, 'class' => 'wowguild-progression-manual-block-raid'));
    }
    $rows[] = $row;
    foreach ($raid->encounters as $encounter) {
      if ($encounter->enable) {
        $row = array(array('data' => theme('image', array('path' => $encounter->icon)), 'class'=>'wowguild-progression-manual-block-icon'), array('data' => t($encounter->name)));
        if (isset($encounter->normal)) {
          if ($encounter->normal) {
            $row[] = array('data' => 'N', 'class' => array('wowguild-progression-manual-block-completed', 'wowguild-progression-manual-block-normal'));
          }
          else {
            $row[] = array('data'=>'N', 'class' => array('wowguild-progression-manual-block-notcompleted', 'wowguild-progression-manual-block-normal'));
          }
        }
        else {
          $row[] = array('data'=>'<div>N</div>', 'class' => 'wowguild-progression-manual-block-hidden');
        }
        if (isset($encounter->heroic)) {
          if ($encounter->heroic) {
            $row[] = array('data' => 'H','class'=>array('wowguild-progression-manual-block-completed','wowguild-progression-manual-block-heroic'));
          }
          else {
            $row[] = array('data' => 'H','class'=>array('wowguild-progression-manual-block-notcompleted','wowguild-progression-manual-block-heroic'));
          }
        }
        else {
          $row[] = array('data'=>'<div>H</div>', 'class' => 'wowguild-progression-manual-block-hidden');
        }
        $rows[] = $row;
      }
    }
    echo theme('table', array('header'=>$head,'rows' => $rows));
  }
}
?>
</div>

