<?php
// $Id$
/**
 * @file
 * Defines the wowguild entity, and functions and class related to it.
 */

/**
 * Called from hook_entity_info().
 */
function wowtoon_entity_wowguild_info() {
  $return = array(
    'wowguild' => array(
      'label' => t('WoW Guild'),
      'controller class' => 'WowGuildController',
      'base table' => 'wowguild_entity',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'gid',
      ),
      'bundles' => array(
        'wowguild' => array(
          'label' => t('WoWGuild'),
          'admin' => array(
            'path' => 'admin/config/wowguild/wowguild',
            'access arguments' => array('access content'),
          ),
        ),
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'view modes' => array(
        'full' => array(
          'label' => t('Guild'),
          'custom settings' => FALSE,
        ),
        'banner' => array(
          'label' => t('banner'),
          'custom settings' => FALSE,
        ),
        'popup' => array(
          'label' => t('popup'),
          'custom settings' => FALSE,
        ),
        'teaser' => array(
          'label' => t('teaser'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
  return $return;  
}


/**
 * Main class for wowguild entities.
 */
class WowGuildController extends DrupalDefaultEntityController {
  /*
   * Make variables more accessable.
   */
  protected function attachLoad(&$guilds, $revision_id = FALSE) {
    parent::attachLoad($guilds, $revision_id);
    foreach ($guilds as $key => $value) {
      if (!empty($guilds[$key]->raid_progression)) {
        $guilds[$key]->raid_progression = unserialize($guilds[$key]->raid_progression);
      }
      if (!empty($guilds[$key]->tabard)) {
        $guilds[$key]->tabard = unserialize($guilds[$key]->tabard);
      }
      $guilds[$key]->nameatrealm = $guilds[$key]->name . '@' . $guilds[$key]->realm;
    }
  }
}

/**
 * Load multiple test entities based on certain conditions.
 *
 * @param $pids
 *   An array of entity IDs.
 * @param $conditions
 *   An array of conditions to match against the {entity} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of test entity objects, indexed by pid.
 */
function wowguild_load_multiple($gids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('wowguild', $gids, $conditions, $reset);
}

/**
 * hook for menu %wowtoon items
 * 
 * @param unknown_type $tid
 */
function wowguild_load($gid, $reset = FALSE) {
  if (!empty($gid)) {
    $guilds = wowguild_load_multiple(array($gid), array(), $reset);
    return $guilds?reset($guilds):FALSE;
  }
  else {
    return FALSE;
  }
}
/**
 * 
 * @param string $realm - Realm of guild to load.
 * @param string $name - Name of guild to load.
 * 
 * @return
 *   {wowguild} entity of guild or FALSE.
 */
function wowguild_load_by_realm_name($realm = NULL, $name = NULL) {
  $guild_key = wowtoon_getkey($realm, $name);
  $guilds = wowguild_load_multiple(array(), array('guild_key' => $guild_key));
  return reset($guilds);
}

/**
 * Load members of guild.
 */
function wowguild_load_members(&$guild) {
  $tids = db_query("SELECT tid FROM {wowtoon_entity} WHERE gid = :gid", array(':gid' => $guild->gid))->fetchCol();
  $guild->members = wowtoon_load_multiple($tids);
}

/**
 * Save {wowguild} entity.
 */
function wowguild_save(&$edit) {
  $edit->guild_key = wowtoon_getkey($edit->realm, $edit->name);
  field_attach_presave('wowguild', $edit);
  
  if (!empty($edit->gid)) {
    drupal_write_record('wowguild_entity', $edit, 'gid');
    field_attach_update('wowguild', $edit);
    module_invoke_all('entity_update', 'wowguild', $edit);
    return $edit;
  }
  drupal_write_record('wowguild_entity', $edit);
  field_attach_insert('wowguild', $edit);
  module_invoke_all('entity_insert', 'wowguild', $edit);
  return $edit;
}

/**
 * Build content for specified view_mode
 * 
 * @param wowguild $node
 * @param string $view_mode
 *   - full, teaser, popup, banner
 * @param string $langcode
 */
function wowguild_build_content(&$guild, $view_mode = 'full', $langcode = NULL) {
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Remove previously built content, if exists.
  $guild->content = array();

  // Build fields content.
  // In case of a multiple view, node_view_multiple() already ran the
  // 'prepare_view' step. An internal flag prevents the operation from running
  // twice.
  field_attach_prepare_view('wowguild', array($guild->gid => $guild), $view_mode);
  entity_prepare_view('wowguild', array($guild->gid => $guild));
  $guild->content += field_attach_view('wowguild', $guild, $view_mode, $langcode);

  // Allow modules to make their own additions to the node.
  module_invoke_all('wowguild_view', $guild, $view_mode, $langcode);
  module_invoke_all('entity_view', $guild, 'wowguild', $view_mode, $langcode);
}

function wowguild_page_title($guild) {
  if (isset($guild->name)) {
    return $guild->name . ' on ' . $guild->realm;
  }
}

function wowguild_lookup($realm, $name) {
  $guild = wowguild_load_by_realm_name($realm, $name);
  if ($guild) {
    drupal_goto('guild/' . $guild->gid);
  }
  else {
    return t('%name on %realm could not be found', array('%name' => $name, '%realm' => $realm));
  }
}

/**
 * View a specific guild.
 * 
 * @param {wowguild} $guild
 * @param string $view_mode
 * @param string $langcode
 */
function wowguild_view($guild, $view_mode = 'full', $langcode = NULL) {
  if ($guild->updated == 0) {
    module_load_include('inc', 'wowtoon', 'wowtoon.update');
    $guild = wowguild_update_armory($guild->gid);
    wowguild_update_feed($guild);
    wowguild_update_progression($guild);
    $guild = wowguild_load($guild->gid, TRUE);
  }
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Populate $node->content with a render() array.
  wowguild_build_content($guild, $view_mode, $langcode);

  $build = $guild->content;
  // We don't need duplicate rendering info in node->content.
  unset($guild->content);

  $build = array_merge($build, array(
    '#theme' => 'wowguild',
    '#guild' => $guild,
    '#view_mode' => $view_mode,
    '#language' => $langcode,
  ));
  if (module_exists('views')) {
    $build['members_view']['#weight'] = 20;
    if (module_exists('wowguild') && variable_get('wowguild_guild_gid', -999) == $guild->gid) {
      $build['members_view']['#markup'] = views_embed_view('toon_views', 'guild_roster', $guild->gid);
    } 
    else {
      $build['members_view']['#markup'] = views_embed_view('toon_views', 'display_guild', $guild->gid);
    }
  }

  // Allow modules to modify the structured node.
  $type = 'wowguild';
  drupal_alter(array('wowguild_view', 'entity_view'), $build, $type);

  return $build;
}

/**
 * Create guild tabard image.  [UNUSED!!!!]
 * 
 * @TODO: Use imagemagick to layer guild tabard image.
 * GD2 does not load transparent images correctly.
 */
function wowtoon_guild_tabard_img() {
  header('Content-type: image/png');
  
  $im = imagecreatetruecolor(240, 240);
  
  // Tell #000 to be transparent.
  $bgc = imagecolorallocate($im, 0, 0, 0);
  imagecolortransparent($im, $bgc);
  //imagealphablending($im, true);
  //imagealphablending($im, false);
  //imagesavealpha($im, true);
  /*
  $ring = imagecreatefrompng("http://us.battle.net/wow/static/images/guild/tabards/ring-alliance.png"); // 216 x 216
  $bg = imagecreatefrompng("http://us.battle.net/wow/static/images/guild/tabards/bg_00.png");           // 179 x 210
  $shadow = imagecreatefrompng("http://us.battle.net/wow/static/images/guild/tabards/shadow_00.png");
  $overlay = imagecreatefrompng("http://us.battle.net/wow/static/images/guild/tabards/overlay_00.png"); // 179 x 159
  $border = imagecreatefrompng("http://us.battle.net/wow/static/images/guild/tabards/border_00.png");
  $emblem = imagecreatefrompng("http://us.battle.net/wow/static/images/guild/tabards/emblem_126.png");  // 125 x 125
  $hooks = imagecreatefrompng("http://us.battle.net/wow/static/images/guild/tabards/hooks.png");        // 179 x  32
  */
  $ring = imagecreatefrompng(dirname(__FILE__) . "/images/guildtabard/ring-alliance.png"); // 216 x 216
  $bg = imagecreatefrompng(dirname(__FILE__) . "/images/guildtabard/bg_00.png");           // 179 x 210
  $shadow = imagecreatefrompng(dirname(__FILE__) . "/images/guildtabard/shadow_00.png");
  $overlay = imagecreatefrompng(dirname(__FILE__) . "/images/guildtabard/overlay_00.png"); // 179 x 159
  $border = imagecreatefrompng(dirname(__FILE__) . "/images/guildtabard/border_00.png");
  $emblem = imagecreatefrompng(dirname(__FILE__) . "/images/guildtabard/emblem_126.png");  // 125 x 125
  $hooks = imagecreatefrompng(dirname(__FILE__) . "/images/guildtabard/hooks.png");        // 179 x  32
  
  
  //$text_color = imagecolorallocate($im, 233, 14, 91);
  //imagestring($im, 1, 5, 5,  'A Simple Text String', $text_color);
  
  //imagecopymerge($im,$ring, 0, 0, 0, 0, 216, 216, 100);
  //imagecopyresampled($im,$ring, 0, 0, 0, 0, 216, 216, 216, 216);
  //imagecopyresampled($im,$border, 0, 0, 0, 0, 179, 159, 179, 159);
  
  /*
  // Set the brush
  imagesetbrush($im, $ring);
  // Draw a couple of brushes, each overlaying each
  imageline($im, imagesx($im) / 2, imagesy($im) / 2, imagesx($im) / 2, imagesy($im) / 2, IMG_COLOR_BRUSHED); 
  // Set the brush
  
  //imagecopymerge($im,$bg, 0, 0, 0, 0, 179, 210, 100);
  
  imagesetbrush($im, $border);
  imageline($im, imagesx($im) / 2, imagesy($im) / 2, imagesx($im) / 2, imagesy($im) / 2, IMG_COLOR_BRUSHED); 
  
  imagecopymerge($im,$emblem, 0, 0, 0, 0, 125, 125, 100);
  imagecopymerge($im,$hooks, 0, 0, 0, 0, 179, 32, 100);
  */
  imagepng($emblem);
  imagedestroy($im);
  imagedestroy($ring);
  imagedestroy($shadow);
  imagedestroy($bg);
  imagedestroy($overlay);
  imagedestroy($border);
  imagedestroy($emblem);
  imagedestroy($hooks);
}

function wowtoon_admin_wowguild() {
    $header = array(
    array('data' => t('Name'), 'field' => 'g.name', 'sort' => 'asc'),
    array('data' => t('Realm'), 'field' => 'g.realm'),
    // array('data' => t('Owner'), 'field' => 'u.username'),  // Guilds cannot be owned (yet)
    'Actions'
  );
  
  $query = db_select('wowguild_entity', 'g')->extend('PagerDefault')->extend('TableSort')->orderByHeader($header);
  $query->fields('g', array('gid', 'name', 'realm'));
  $query->leftJoin('users', 'u', 'u.uid = g.uid');
  $query->fields('u', array('name', 'uid'));
  /*
  $query->join('field_data_wowtoon_name', 'n', 'n.entity_id = g.gid AND n.entity_type = :entity_type AND n.deleted = 0', array(':entity_type' => 'wowguild'));
  $query->fields('n', array('wowtoon_name_value'));
  $query->join('field_data_wowtoon_realm', 'r', 'r.entity_id = g.gid AND r.entity_type = :entity_type AND r.deleted = 0', array(':entity_type' => 'wowguild'));
  $query->fields('r', array('wowtoon_realm_value'));
  */
  $query->limit(10);
  $results = $query->execute();
  
  
  $rows = array();
  foreach ($results as $guild) {
    $row = array();
    $row[] = l($guild->name, 'guild/' . $guild->gid);
    $row[] = $guild->realm;
    
    // Guilds cannot be owned (yet)
    //$row[] = l($guild->name, 'user/' . $guild->uid);
    
    $actions = array();
    $actions[] = l(t('view'), 'guild/' . $guild->gid);
    $row[] = implode(' ', $actions);
    $rows[] = $row;
  }
  
  $build['table'] = array(
    '#prefix' => '<h3>' . t('WoW Guilds') . '</h3>',
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );
  return $build;
}