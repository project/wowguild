<?php
// $Id$
/**
 * @file
 * Default theme implementation for wowguilds.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) profile type label.
 * - $url: The URL to view the current profile.
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - profile-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $content['wowtoon_name'] - Name of guild field.
 * - $content['wowtoon_realm'] - The guild realm field.
 * - $content['wowtoon_battlegroup'] - Guild's battlegroup.
 * - $content['wowtoon_level'] - The level of the guild.
 * - $content['members_view'] - The list of members in the guild.  This is a 
 *   views render configureable at [admin/structure/views/edit/toon_views] if you have views_ui module enabled.
 * - $members_count = XX members
 * - $guild_tabard_div - CANVAS or DIV element for the guild tabard.
 * - $guild_level_text - Level {LEVEL} guild on the realm {REALM}, {COUNT} members
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
hide($content['wowtoon_battlegroup']);
hide($content['wowtoon_realm']);
hide($content['wowtoon_level']);
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="content wowguild "<?php print $content_attributes; ?>>
    <div class="guild-tabard-wrapper">
      <?php echo $guild_tabard_div; ?>
      <?php echo render($content['wowtoon_name']);?>
      <div class="wowguild-level-members">
      <?php echo $guild_level_text;?>
      </div>
      <div class="updated"><?php echo $updated; ?></div>
    </div>
    <?php
      print render($content);
    ?>
  </div>
</div>