<?php
/**
 * @file
 * Used when displaying toons via a mouseover popup.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) profile type label.
 * - $url: The URL to view the current profile.
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - profile-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $validated: (boolean) has this character been validated.
 * - $validated_img: checkmark denoting if character has been validated.
 * - $guild_name: Name of Guild if available.
 * - $guild_link: Link to toon's guild.
 * - $updated_interval: Updated XXX hours ago.
 * 
 * @see template_preprocess_wowtoon()
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

// We will be rendering these manually.
hide($content['wowtoon_name']);
hide($content['wowtoon_realm']);
hide($content['wowtoon_fullname']);
hide($content['wowtoon_guild_name']);
hide($content['wowtoon_avg_ilevel']);
hide($content['wowtoon_avg_ilevel_best']);
hide($content['wowtoon_level']);
hide($content['wowtoon_battlegroup']);
hide($content['wowtoon_achievement_points']);


?>

<div class="summary-popup <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<?php if ($set): ?>
<img src="<?php echo $avatar; ?>" width="52" height="52" class="avatar" />
<?php endif; ?>
  <div class="popup-frame">
  <div class="popup-text-wrapper">
    <div class="name"><?php print $title; ?></div>
    <?php if ($guild_name) {
      echo '<div class="guild">' . $guild_name . "</div>";
    }
    ?>
    <div class="level-class-race color-c<?php echo $toon->classid; ?>">
      <strong><?php echo $content['wowtoon_level'][0]['#markup']; ?></strong>
      <?php echo $toon->race; ?>
      <?php echo $toon->class; ?>,
      <?php echo $toon->realm; ?>
    </div>
    
    
    <?php if ($set) {?>
    <div class="achievements-spec-ilevel">
    <span class="achievements"><?php echo $achievement_points; ?></span>
    <span class="spec"><?php echo $set->spec; ?></span>
    <span class="ilevel" title="Equiped Average iLevel: <?php echo $avg_ilevel; ?>">avg ilevel: <?php echo $avg_ilevel_best; ?></span>
    </div>
    <?php } else { ?>
    <div class="achievements-spec-ilevel">Character not loaded</div>
    <?php } ?>
  </div></div>
    <?php
      // We've printed all we need.  If we print the rest, any added fields get appended.
      // print render($content);
    ?>
</div>









