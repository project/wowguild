<?php
// $Id$

/**
 * @file
 * Default theme implementation for wowtoons.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) profile type label.
 * - $url: The URL to view the current profile.
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - profile-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $validated: (boolean) has this character been validated.
 * - $validated_img: checkmark denoting if character has been validated.
 * - $guild_name: Name of Guild if available.
 * - $guild_link: Link to toon's guild.
 * - $updated_interval: Updated XXX hours ago.
 * 
 * @see template_preprocess_wowtoon()
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

// We will be rendering these manually.
hide($content['wowtoon_name']);
hide($content['wowtoon_realm']);
hide($content['wowtoon_fullname']);
hide($content['wowtoon_guild_name']);
hide($content['wowtoon_avg_ilevel']);
hide($content['wowtoon_avg_ilevel_best']);
hide($content['wowtoon_level']);
hide($content['wowtoon_battlegroup']);
hide($content['wowtoon_achievement_points']);

?>

<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="content"<?php print $content_attributes; ?>>
    <!--  Copied from Teaser -->
    <div class="summary-teaser">
      <?php if ($guild_link): ?>
      <div class="guild"><?php echo $guild_link; ?></div>
      <?php endif; ?>
      <div class="level-class-race shadow color-c<?php echo $classid; ?>">
        <strong><?php echo $level; ?></strong>
        <?php echo $race; ?>
        <?php echo $class; ?>,
        <?php echo $realm; ?>
      </div>
      
      <div class="achievements-spec-ilevel">
      <span class="achievements"><?php echo $achievement_points; ?></span>
      <?php if ($set) :?><span class="spec"><?php echo $spec; ?></span><?php endif; ?>
      <span class="ilevel" title="Equiped Average iLevel: <?php echo $avg_ilevel; ?>">avg ilevel: <?php echo $avg_ilevel_best; ?></span>
      <?php if ($validated_img): ?>
      <span class="validated-image"><?php echo $validated_img; ?></span>
      <?php endif; ?>
      </div>
    </div>
    Updated <?php echo $updated_interval; ?>
    <!--  END:Copied from Teaser -->
    <?php if ($toggle_item_details) { echo $toggle_item_details; }?>
    <?php echo render($content['items']); ?>
    <?php echo render($content['specs']); ?>
    <?php echo render($content['stats']); ?>

    <?php
    print render($content); 
    ?>
  </div>
</div>
