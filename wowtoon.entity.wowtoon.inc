<?php
// $Id$
/**
 * @file
 * Defines the wowguild entity, and functions and class related to it.
 */

/**
 * Called from hook_entity_info().
 */
function wowtoon_entity_wowtoon_info() {
  $return = array(
    'wowtoon' => array(
      'label' => t('WoW Toon'),
      'controller class' => 'WowToonController',
      'base table' => 'wowtoon_entity',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'tid',
      ),
      'bundles' => array(
        'wowtoon' => array(
          'label' => t('WoWToon'),
          'admin' => array(
            'path' => 'admin/config/wowguild/wowtoon',
            'access arguments' => array('access content'),
          ),
        ),
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'view modes' => array(
        'full' => array(
          'label' => t('Toon'),
          'custom settings' => FALSE,
        ),
        'teaser' => array(
          'label' => t('teaser'),
          'custom settings' => FALSE,
        ),
        'postedby' => array(
          'label' => t('Posted By'),
          'custom settings' => FALSE,
        ),
        'banner' => array(
          'label' => t('banner'),
          'custom settings' => FALSE,
        ),
        'popup' => array(
          'label' => t('popup'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
  return $return;  
}


/**
 * Load multiple test entities based on certain conditions.
 *
 * @param $pids
 *   An array of entity IDs.
 * @param $conditions
 *   An array of conditions to match against the {entity} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of test entity objects, indexed by pid.
 */
function wowtoon_load_multiple($tids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('wowtoon', $tids, $conditions, $reset);
}

/**
 * hook for menu %wowtoon items
 * 
 * @param integer $tid
 * @param bool $reset - Don't use cache
 */
function wowtoon_load($tid, $reset = FALSE) {
  $toons = wowtoon_load_multiple(array($tid), array(), $reset);
  return $toons?reset($toons):FALSE;
}

/**
 * This function will take a realm and name, and return a JSON encoded toon if valid. 
 */
function wowtoon_ajax_load_by_realm_name($realm = NULL, $name = NULL) {
  module_load_include('inc', 'wowtoon', 'wowtoon.update');
  // Check that toon exists in the armory... but do not claim it yet.
  $toon = wowtoon_update_armory($realm, $name);
  drupal_json_output($toon);
}

/**
 * This is a generic #autocomplete_path form item lookup for toons that are in the database by the user typing name and realm.
 */
function wowtoon_ajax_lookup_name_at_realm($string) {
  // See if user is typing the realm.
  list ($name, $realm) = explode('@', $string);
  
  if (empty($realm)) {
    // User has not specified realm.  Use their currently selected toon's realm.
    $toon = wowtoon_get_current_toon();
    if ($toon) {
      $realm = $toon->realm;
    }
    $query = db_select('wowtoon_entity', 't');
    $query->fields('t', array('tid'));
    $query->join('field_data_wowtoon_name', 'n', "t.tid = n.entity_id AND n.entity_type = 'wowtoon' AND n.deleted = 0");
    $query->fields('n', array('wowtoon_name_value'));
    $query->join('field_data_wowtoon_realm', 'r', "t.tid = r.entity_id AND r.entity_type = 'wowtoon' AND r.deleted = 0");
    $query->fields('r', array('wowtoon_realm_value'));
    $query->condition('n.wowtoon_name_value', '%' . $name . '%', 'LIKE');
    if ($realm) {
      $query->condition('r.wowtoon_realm_value', $realm);
    }
    $results = $query->execute();
    
    $output = array();
    foreach ($results as $result) {
      $output[$result->wowtoon_name_value . '@' . $result->wowtoon_realm_value] = check_plain($result->wowtoon_name_value . '@' . $result->wowtoon_realm_value);
    }
    drupal_json_output($output); 
  }
  else {
    // User has entered part of the realm, autocomplete it.
    $all_realms = _wowtoon_get_realms();
    $output = array();
    foreach ($all_realms as $ele) {
      if (strpos(strtolower($ele), strtolower($realm)) !== FALSE) {
        $output[$name . '@' . $ele] = check_plain($name . '@' . $ele);
      }
    }
    drupal_json_output($output);
  }
}

function wowtoon_load_by_realm_name($realm = NULL, $name = NULL) {
  $toon_key = wowtoon_getkey($realm, $name);
  $toons = wowtoon_load_multiple(array(), array('toon_key' => $toon_key));
  return reset($toons);
}

/**
 * Save wowtoon entity
 */
function wowtoon_save(&$edit) {
  if (!isset($edit->fullname)) {
    $edit->fullname = $edit->name;
  }
  $edit->toon_key = wowtoon_getkey($edit->realm, $edit->name);
  field_attach_presave('wowtoon', $edit);
  
  module_invoke_all('wowtoon_presave', $edit);
  module_invoke_all('entity_presave', $edit, 'wowtoon');
  
  if (!empty($edit->tid)) {
    drupal_write_record('wowtoon_entity', $edit, 'tid');
    field_attach_update('wowtoon', $edit);
    module_invoke_all('entity_update', $edit, 'wowtoon');
  }
  else {
    drupal_write_record('wowtoon_entity', $edit);
    field_attach_insert('wowtoon', $edit);
    module_invoke_all('entity_insert', $edit, 'wowtoon');
  }

  if (!empty($edit->uid)) {
    // See if we are claiming a new toon.
    $result = db_query('SELECT tid, uid FROM {wowtoon_owners} WHERE tid = :tid', array(':tid' => $edit->tid))->fetchObject();
    // If new, or this is a new owner, delete old data, and insert new.
    if (empty($result->tid) || $result->uid != $edit->uid) {
      db_delete('wowtoon_owners')
        ->condition('tid', $edit->tid)
        ->execute();
      db_insert('wowtoon_owners')
        ->fields(array(
          'tid' => $edit->tid,
          'toon_key' => $edit->toon_key,
          'status' => isset($edit->status)?$edit->status:0,
          'uid' => $edit->uid,
        ))
        ->execute();
    }
    else {
      db_update('wowtoon_owners')
        ->fields(array(
          'uid' => $edit->uid,
          'status' => isset($edit->status)?$edit->status:0,
        ))
        ->condition('tid', $edit->tid)
        ->execute();
    }
  }
  else {
    // Make sure there are no personal settings left over if the owner is being removed.
    db_delete('wowtoon_owners')
        ->condition('tid', $edit->tid)
        ->execute();
  }
  
  return $edit;
}
/**
 * 'Delete' wowtoon.
 * if $force is TRUE, delete the entity, otherwise clear user settings and 'un owns' toon.
 */
function wowtoon_delete($tid, $force = FALSE) {
  if (is_numeric($tid)) {
    $toon = wowtoon_load($toon);
  }
  else {
    $toon = $tid;
  }
  if (empty($toon->tid)) {
    return FALSE;
  }
  db_delete('wowtoon_owners')
    ->condition('tid', $toon->tid)
    ->execute();
  
  if ($force === TRUE) {
    // Unclaim toon.
    db_delete('wowtoon_entity')
      ->condition('tid', $toon->tid)
      ->execute();
    db_delete('wowtoon_sets')
      ->condition('tid', $toon->tid)
      ->execute();
    db_delete('wowtoon_saved_achievements')
      ->condition('tid', $toon->tid)
      ->execute();
    watchdog('wowtoon', 'Removed !toon from associated account.', array('!toon' => l($toon->nameatrealm)));
  }
  
  // Delete Personal Settings
  variable_del('wowtoon_blocks_settings_tid_' . $toon->tid);
  module_invoke_all('entity_delete', $toon, 'wowtoon');
}

/**
 * Main class for wowtoon entities.
 */
class WowToonController extends DrupalDefaultEntityController {
  protected function attachLoad(&$toons, $revision_id = FALSE) {
    
    foreach ($toons as $key => $toon) {
      $toons[$key]->professions = unserialize($toon->professions);
      $toons[$key]->progression = unserialize($toon->progression);
      $toons[$key]->audit = unserialize($toon->audit);
      $toons[$key]->nameatrealm = $toons[$key]->name . '@' . $toons[$key]->realm;
      $toons[$key]->status = 0;
    }
    
    $result = db_query("SELECT uid, toon_key, tid, status FROM {wowtoon_owners} WHERE tid IN(:tids)", array(':tids' => array_keys($toons)));
    foreach ($result as $record) {
      $toons[$record->tid]->uid = $record->uid; 
      $toons[$record->tid]->status = $record->status;
    }
    
    $result = db_query("SELECT * FROM {wowtoon_sets} WHERE tid IN(:tids) ORDER BY set_id ASC", array(':tids' => array_keys($toons)));
    foreach ($result as $record) {
      $toons[$record->tid]->sets[$record->set_id] = $record;
      $toons[$record->tid]->sets[$record->set_id]->items = unserialize($record->items);
      $toons[$record->tid]->sets[$record->set_id]->allstats = unserialize($record->allstats);
      $toons[$record->tid]->sets[$record->set_id]->stats = unserialize($record->stats);
      if ($record->active == 1) {
        $toons[$record->tid]->active_set_id = $record->set_id;
      }
    }
    
    // Let fields attach themselves
    parent::attachLoad($toons, $revision_id);
  }
  
  /**
   * Specifies the default label, which is picked up by label() by default.
   */
  protected function defaultLabel() {
    return $this->name;
  }

  /**
   * Specifies the default uri, which is picked up by uri() by default.
   */
  protected function defaultURI() {
    return array('path' => 'toon/' . $this->identifier());
  }
}



/**
 * Build content for specified view_mode
 * 
 * @param unknown_type $node
 * @param unknown_type $view_mode
 *   - full, teaser, popup, banner
 * @param unknown_type $langcode
 */
function wowtoon_build_content(&$toon, $view_mode = 'full', $langcode = NULL) {
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }

  // Remove previously built content, if exists.
  $toon->content = array();

  // Build fields content.
  // In case of a multiple view, node_view_multiple() already ran the
  // 'prepare_view' step. An internal flag prevents the operation from running
  // twice.
  field_attach_prepare_view('wowtoon', array($toon->tid => $toon), $view_mode);
  entity_prepare_view('wowtoon', array($toon->tid => $toon));
  $toon->content += field_attach_view('wowtoon', $toon, $view_mode, $langcode);

  $details = FALSE;
  if ($view_mode == 'full') {
    if (variable_get('wowtoon_allow_item_details', 1)) {
      if (isset($_GET['details'])) {
        $_SESSION['wowtoon_detailed_items'] = $_GET['details'] == 1;
      }
      if (isset($_SESSION['wowtoon_detailed_items']) && $_SESSION['wowtoon_detailed_items'] === TRUE) {
        $details = TRUE;
      }
    }
    
    // Sets will not be set if there's an error with the toon.
    if ($toon->sets) {
      // See if we are viewing a different spec.
      if (isset($_GET['spec'])) {
        // Make sure the spec exists and we have loaded it.
        if (array_key_exists($_GET['spec'], $toon->sets) && $toon->sets[$_GET['spec']]->allstats !== FALSE) {
          $set = $toon->sets[$_GET['spec']];
        }
        else {
          $set = $toon->sets[$toon->active_set_id];
        }
      }
      else {
        $set = $toon->sets[$toon->active_set_id];
      }
      $toon->content['items'] = array(
        '#theme' => 'wowtoon_items',
        '#toon' => $toon,
        '#set' => $set,
        '#details' => $details
      );
      $toon->content['specs'] = array(
        '#theme' => 'wowtoon_specs',
        '#toon' => $toon,
        '#set' => $set,
      );
          $toon->content['stats'] = array(
        '#theme' => 'wowtoon_stats',
        '#toon' => $toon,
        '#set' => $set,
      );
    }
  }
  if ($view_mode == 'teaser') {
  }
  // Allow modules to make their own additions to the node.
  module_invoke_all('wowtoon_view', $toon, $view_mode, $langcode);
  module_invoke_all('entity_view', $toon, 'wowtoon', $view_mode, $langcode);
}

/**
 * Supply the page title for wowtoons.
 */
function wowtoon_page_title($toon) {
  if (!empty($toon->fullname)) {
    return $toon->fullname . ' on ' . $toon->realm;
  }
}

/**
 * Allows users to jump to toons via name / realm.
 * 
 * @param string $realm
 * @param string $name
 */
function wowtoon_lookup($realm, $name) {
  $toon = wowtoon_load_by_realm_name($realm, $name);
  if ($toon) {
    drupal_goto('toon/' . $toon->tid);
  }
  else {
    return t('%name on %realm could not be found', array('%name' => $name, '%realm' => $realm));
  }
}

/**
 * Display a wowtoon.
 * 
 * @param {wowtoon} $toon
 * @param string $view_mode
 * @param string $langcode
 */
function wowtoon_view($toon, $view_mode = 'full', $langcode = NULL) {
  // Verify that we've done our first load
  if ($toon->update_attempted == 0 && $view_mode == 'full') {
    module_load_include('inc', 'wowtoon', 'wowtoon.update');
    $toon = wowtoon_update_armory($toon->tid);
    if (!empty($toon->tid)) {
      wowtoon_update_feed($toon);
    }
    else {
      // An error was returned by wowtoon_update_armory().
      //print "asdasd" . $toon . ']';
      //$toon->update_status = (integer)$toon;
      //print "asdasd" . $toon->update_status . ']';
    }
    $toon = wowtoon_load($toon->tid, TRUE);
  }
  
  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }
  
  
  // Check for errors from updates.
  if ($toon->update_status == WOWTOON_UPDATE_CHARACTER_TOO_LOW && $view_mode == 'full') {
    $build['#markup'] = t('This character is too low to be listed on the Armory.  You must be level 10 or above.');
  } elseif ($toon->update_status == WOWTOON_UPDATE_CHARACTER_NOT_AVAILABLE && $view_mode == 'full') {
    $build['#markup'] = t('This character is not available on the Armory, possibly for one of the following reasons:<br/>
<ul>
<li>The character has been inactive for an extended period of time.</li>
<li>The character name or realm was spelled incorrectly.</li>
<li>The profile is temporarily unavailable while the character is in the midst of a process such as a realm transfer or faction change.</li>
<li>Characters that have been deleted are no longer available.</li>
</ul>');
  } elseif ($toon->update_status == WOWTOON_UPDATE_ARMORY_DOWN && $view_mode == 'full') {
    $build['#markup'] = t('This character is not loaded and the armory appears to be down.  Please try again later.');
  } elseif ($toon->update_status == WOWTOON_UPDATE_INVALID_PAGE && $view_mode == 'full') {
    $build['#markup'] = t('This character is not loaded and we received an invalid page from the armory.  Please try again later.');
  }
  else {
    // Populate $toon->content with a render() array.
    wowtoon_build_content($toon, $view_mode, $langcode);
  
    $build = $toon->content;
    // We don't need duplicate rendering info in node->content.
    unset($toon->content);
    $build = array_merge($build, array(
      '#theme' => 'wowtoon',
      '#toon' => $toon,
      '#view_mode' => $view_mode,
      '#language' => $langcode,
    ));
  
    // Allow modules to modify the structured node.
    $type = 'wowtoon';
    drupal_alter(array('wowtoon_view', 'entity_view'), $build, $type);
  }
  if ($view_mode == 'popup') {
    echo drupal_render($build);
    die();
    return FALSE;
  }
  elseif ($view_mode == 'ajax') {
    drupal_json_output($toon);
    die();
    return FALSE;
  }
  else {
    return $build;
  }
}  
  

/**
 * Display the Add Character Form.
 * 
 * If $ajax == TRUE, output the HTML form, otherwise, return it.
 */
function wowtoon_add_to_account($ajax = FALSE) {
  global $user;
  
  if (is_numeric(arg(2))) {
    $wowtoon = wowtoon_load(arg(2));
    $wowtoon->uid = $user->uid;
  }
  else {
    $wowtoon = (object)array (
      'type' => 'wowtoon',
      'validation_required' => FALSE,
      'uid' => $user->uid
    );
  }
  if ($ajax == 'ajax') {
    echo drupal_render(drupal_get_form('wowtoon_add_form', $wowtoon));
    die();
  }
  else {
    return drupal_get_form('wowtoon_add_form', $wowtoon);
  }
}

/**
 * Allows a user to find a character on the armory or local database.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @param unknown_type $wowtoon
 */
function wowtoon_find_form($form, &$form_state) {
  $form['name'] = array(
    '#title' => t('Character Name'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => NULL,
  );
  $form['realm'] = array(
    '#title' => t('Character Realm'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => !empty($_SESSION['saved_realm'])?$_SESSION['saved_realm']:'',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#weight' => 100,
    '#value' => t('Find Character')
  );
  return $form;
}

/**
 * Verify name / realm and existance in the database or armory.
 */
function wowtoon_find_form_validate($form, &$form_state) {
  global $user;
  $wowtoon_submission = (object) $form_state['values'];
  
  if (empty($wowtoon_submission->name)) {
    form_set_error('name', t('Name cannot be blank.'));
  }
  elseif (_wowtoon_validate_realm($wowtoon_submission->realm) === FALSE) {
    form_set_error('realm', t('@realm is not a valid World of Warcraft realm.', array('@realm' => $realm)));
  }
  else {
  
    $realm = trim($wowtoon_submission->realm);
    $name = trim($wowtoon_submission->name);
    // Check the database then the armory.
    module_load_include('inc', 'wowtoon', 'wowtoon.update');
    // Check that toon exists in the armory... but do not claim it yet.
    $toon = wowtoon_update_armory($realm, $name);
    
    if (is_numeric($toon)) {
      // Error
      switch ($toon) {
        case WOWTOON_UPDATE_INVALID_PAGE:
          form_set_error('name', t('We received an invalid page from the Armory!'));
          break;
          case WOWTOON_UPDATE_ARMORY_DOWN:
          form_set_error('name', t('We could not contact the World of Warcraft Armory, it may be down.'));
          break;
        case WOWTOON_UPDATE_CHARACTER_TOO_LOW:
          form_set_error('name', t('@name exists, but must be more than level 10 to add.', array('@name' => $name)));
          break;
        case WOWTOON_UPDATE_CHARACTER_NOT_AVAILABLE:
          form_set_error('name', t('@name could not be found on the World of Warcraft Armory.  Verify that is exists on battle.net.', array('@name' => $name)));
          break;
      }
    }
    else {
      $form_state['toon'] = $toon;
    }
  }
}
function wowtoon_find_form_submit($form, &$form_state) {
  $toon = $form_state['toon'];
  $_SESSION['saved_realm'] = $toon->realm;
  drupal_goto('toon/' . $toon->tid);
}
/**
 * This form will create {wowtoon} entity objects.
 * 
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @param unknown_type $wowtoon
 *   The following object should be passed to drupal_get_form:
 *   $wowtoon->tid = '';
 *   $wowtoon->type = 'wowtoon';
 *   $wowtoon->newuid = 0;
 *     - If newuid is passed, then we are attempting to claim a toon.  A successful submit will add this toon to the user's account.
 *   $wowtoon->validation_required = FALSE
 *     - If TRUE is passed, then WE will require the user to validate this toon (remove a piece of armor to claim)
 *     - If FALSE is passed, then validation will be required ONLY IF someone else has claimed this toon.
 * 
 * return - On a successful submit, the user will be redirected to the new toon.
 */
function wowtoon_add_form($form, &$form_state, $wowtoon) {
  field_attach_form('wowtoon', $wowtoon, $form, $form_state);
  
  // If user has saved a realm, use it, otherwise set empty.
  if (empty($_SESSION['saved_realm'])) {
  	$_SESSION['saved_realm'] = '';
  }
  
  $form['tid'] = array(
    '#type' => 'value',
    '#value' => !empty($wowtoon->tid) ? $wowtoon->tid : NULL,
  );
  if (user_access('assign wowtoon')) {
    $form['username'] = array(
      '#title' => t('Character Owner'),
      '#type' => 'textfield',
      '#default_value' => '',
      '#autocomplete_path' => 'user/autocomplete',
      '#description' => t('Leave blank for un-owned.')
    );
    if (!empty($wowtoon->uid)) {
      $owner = user_load($wowtoon->uid);
      $form['username']['#default_value'] = $owner->name;
    }
  }
  else {
    $form['uid'] = array(
      '#type' => 'value',
      '#value' => !empty($wowtoon->uid) ? $wowtoon->uid : NULL,
    );
  }
  $form['name'] = array(
    '#title' => t('Character Name'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => !empty($wowtoon->name) ? $wowtoon->name : NULL,
  );
  $form['realm'] = array(
    '#title' => t('Character Realm'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => !empty($wowtoon->realm) ? $wowtoon->realm : $_SESSION['saved_realm'],
  );
  
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#weight' => 100,
  );
  if (!empty($wowtoon->tid)) {
    $form['name']['#disabled'] = TRUE;
    $form['realm']['#disabled'] = TRUE;
    $form['submit']['#value'] = t('Save Character');
  }
  elseif (!empty($wowtoon->uid)) {
    $form['submit']['#value'] = t('Claim Character');
    $form['#action'] = url('toon/add');
  }
  else {
    $form['submit']['#value'] = t('Find Character');
  }
  
  return $form;
}

/**
 * Verify name / realm and existance in the database or armory.
 */
function wowtoon_add_form_validate($form, &$form_state) {
  global $user;
  $wowtoon_submission = (object) $form_state['values'];
  
  field_attach_form_validate('wowtoon', $wowtoon_submission, $form, $form_state);
  
  if (!form_get_errors()) {
    if (!empty($wowtoon_submission->username)) {
      $newuser = user_load_by_name($wowtoon_submission->username);
      if (empty($newuser)) {
        form_set_error('username', t('%username could not be found.', array('%username' => $wowtoon_submission->username)));
      }
    }
    
    if (empty($wowtoon_submission->name)) {
      form_set_error('name', t('Name cannot be blank.'));
    }
    elseif (_wowtoon_validate_realm($wowtoon_submission->realm) === FALSE) {
      form_set_error('realm', t('@realm is not a valid World of Warcraft realm.', array('@realm' => $wowtoon_submission->realm)));
    }
    else {
      $realm = trim($wowtoon_submission->realm);
      $name = trim($wowtoon_submission->name);
      // Check the database then the armory.
      module_load_include('inc', 'wowtoon', 'wowtoon.update');
      // Check that toon exists in the armory... but do not claim it yet.
      $toon = wowtoon_update_armory($realm, $name);
      
      if (is_numeric($toon)) {
        // Error
        switch ($toon) {
          case WOWTOON_UPDATE_INVALID_PAGE:
            form_set_error('name', t('We received an invalid page from the Armory!'));
            break;
            case WOWTOON_UPDATE_ARMORY_DOWN:
            form_set_error('name', t('We could not contact the World of Warcraft Armory, it may be down.'));
            break;
          case WOWTOON_UPDATE_CHARACTER_TOO_LOW:
            form_set_error('name', t('@name exists, but must be more than level 10 to add.', array('@name' => $name)));
            break;
          case WOWTOON_UPDATE_INVALID_REALM:
            form_set_error('realm', t('@realm is not a valid World of Warcraft realm.', array('@realm' => $realm)));
            break;
          case WOWTOON_UPDATE_CHARACTER_NOT_AVAILABLE:
            form_set_error('wowtoon_name', t('@name could not be found on the World of Warcraft Armory.  Verify that is exists on battle.net.', array('@name' => $name)));
            break;
        }
      }
      else {
        $form_state['toon'] = $toon;
        if (!empty($wowtoon_submission->uid) && !empty($toon->uid) && $wowtoon_submission->uid != $toon->uid) {
          // We are attempting to claim someone elses toon.
          form_set_error('wowtoon_name', t('@name exists but will require validation to claim.', array('@name' => $name)));
          drupal_goto("toon/" . $toon->tid . "/validate");
        }
      }
    }
  }
}
/**
 * Save wowtoon data.
 */
function wowtoon_add_form_submit($form, &$form_state) {
  global $user;
  $wowtoon_submission = (object) $form_state['values'];
  
  field_attach_submit('wowtoon', $wowtoon_submission, $form, $form_state);
  
  module_load_include('inc', 'wowtoon', 'wowtoon.update');
  $realm = trim($wowtoon_submission->realm);
  $name = trim($wowtoon_submission->name);
  $toon = wowtoon_update_armory($realm, $name);
  $wowtoon_submission->tid = $toon->tid;
  $wowtoon_submission->realm = $toon->realm;
  $wowtoon_submission->name = $toon->name;
  
  $_SESSION['saved_realm'] = $toon->realm;
  
  
      
  if (user_access('assign wowtoon')) {
    if (!empty($wowtoon_submission->username)) {
      $newuser = user_load_by_name($wowtoon_submission->username);
      $wowtoon_submission->uid = $newuser->uid;
    }
    else {
      $wowtoon_submission->uid = 0;
    }
  }
  
  if (!empty($wowtoon_submission->uid)) {
    $toon->uid = $wowtoon_submission->uid;
    if ($user->uid == $wowtoon_submission->uid) {
      // If this user is the one claiming the toon, change it to the selected toon.
      wowtoon_set_current_tid($toon->tid);
    }
  }
  wowtoon_save($wowtoon_submission);
  
  $form_state['redirect'] = "toon/" . $toon->tid;
}



/**
 * Allow a user to claim a character by validating it.  Required IF user tries to claim a character that someone else has claimed.
 */
function wowtoon_validate_toon_form($form, &$form_state, $toon) {
  global $user;

  // Verify that we need to validate.
  if ($toon->status == 1 && $toon->uid == $user->uid) {
    $form['status']['#markup'] = t('You have already validated !toon.', array('!toon' => l($toon->name, 'toon/' . $toon->tid)));
    return $form;
  }
  
  // Store the slot to change in the database so the user can't change it by editing the submit form.
  list($tid, $slot) = explode('_', variable_get('wowtoon_saved_validation_slot_' . $user->uid, '0_0'));
  
  $slotlookup = array(2 => 'Sholders', 7 => 'Boots', 8 => 'Bracers', 9 => 'Gloves');
  
  drupal_set_title(t('Validating @name on @realm', array('@name' => $toon->name, '@realm' => $toon->realm)));  
  
  if ($tid != $toon->tid) {
    // Decide which item to equip / unequip.
    $rnd = rand(0, count($slotlookup)-1);
    $i=0;
    $idx = 0;
    foreach ($slotlookup as $key => $value) {
      if ($i == $rnd) {
        $idx = $key;
      }
      $i++;
    }
    
    $set = $toon->sets[$toon->active_set_id];
    $found = FALSE;
    foreach ($set->items as $key => $value) {
      if ($value['data-id'] == $idx) {
        $found = TRUE;
      }
    }
    $slot = $found?$idx * -1:$idx;
    $tid = $toon->tid;
    variable_set('wowtoon_saved_validation_slot_' . $user->uid, $toon->tid . '_' . $slot);
  }
  
  $set = $toon->sets[$toon->active_set_id];
  foreach ($set->items as $key => $value) {
    if ($value['data-id'] == abs($slot)) {
      if ($slot > 1) {
        $set->items[$key]['equipme'] = 1;
      }
      else {
        $set->items[$key]['unequipme'] = 1;
      }
    }
  }
  
  $output = '';
  if ($toon->uid != $user->uid) {
    $output .= '<p>' . t("The character you have selected has also been selected by another user!  But don't worry, we just need to do a little extra validation.") . '</p>';
  }
  $output .= '<p>' . t("In order to verify that you own this character, please log into the character and") . ' ';
  if ($slot > 0) {
    $output .= t('equip an item');
  }
  else {
    $output .= t('unequip the item');
  }
  $output .= ' ' . t("in your %slot slot.", array('%slot' => $slotlookup[abs($slot)])) . '</p>';
  $output .= '<p>' . t("Once you have finished and logged out of this character, click 'Validate Character' below.") . '</p>';
  
  $form['validation_instructions']['#markup'] = $output;
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Validate Character'),
  );
  $form['validate_items']['#markup'] = theme('wowtoon_items', array('toon' => $toon, 'set' => $set));
    
  return $form;
}

/**
 * Check the armory to see if the user has completed the task.
 */
function wowtoon_validate_toon_form_validate($form, &$form_state) {
  global $user;
  
  $toon = $form_state['build_info']['args'][0];
  
  $slotlookup = array(2 => 'Sholders', 7 => 'Boots', 8 => 'Bracers', 9 => 'Gloves');
  list($tid, $slot) = explode('_', variable_get('wowtoon_saved_validation_slot_' . $user->uid, '0_0'));
  
  if ($tid != $toon->tid) {
    drupal_set_error('', t('Error: TID != TOON->TID'));
  }
  else {
    module_load_include('inc', 'wowtoon', 'wowtoon.update');
    $validate = wowtoon_update_armory($toon->tid, '', $slot);
    if ($validate !== TRUE) {
      if ($slot > 0) {
        form_set_error('', t('We have checked the armory, and we could not find an item in the %slot slot.', array('%slot' => $slotlookup[abs($slot)])));
      }
      else {
        form_set_error('', t('We have checked the armory, and the item in the %slot slot is still equiped.', array('%slot' => $slotlookup[abs($slot)])));
      }
    }
  }
}
/**
 * Add this toon to the users acount and set status = 1 (validated).
 */
function wowtoon_validate_toon_form_submit($form, &$form_state) {
  global $user;
  $toon = $form_state['build_info']['args'][0];
  $toon->uid = $user->uid;
  $toon->status = 1;
  wowtoon_save($toon);
  variable_set('wowtoon_saved_validation_slot_' . $user->uid, '0_0');
  
  wowtoon_set_current_tid($toon->tid);
  $form_state['redirect'] = "toon/" . $toon->tid; 
}

function wowtoon_admin_wowtoon() {
  $header = array(
    array('data' => t('Name'), 'field' => 't.name', 'sort' => 'asc'),
    array('data' => t('Guild'), 'field' => 't.guild_name'),
    array('data' => t('Realm'), 'field' => 't.realm'),
    array('data' => t('Owner'), 'field' => 'u.username'),
    array('data' => t('Validated')),
    'Actions'
  );
  
  $query = db_select('wowtoon_entity', 't')->extend('PagerDefault')->extend('TableSort')->orderByHeader($header);
  $query->fields('t', array('tid', 'raceid', 'factionid', 'classid', 'class', 'race', 'gender', 'genderid', 'gid', 'name', 'realm', 'guild_name'));
  $query->leftJoin('wowtoon_owners', 'o', 'o.tid = t.tid');
  $query->fields('o', array('uid', 'status'));
  $query->leftJoin('users', 'u', 'u.uid = o.uid');
  $query->addField('u', 'name', 'username');
  $query->condition('t.uid', 0, '>');
  $query->limit(10);
  $results = $query->execute()->fetchAll();
  
  $rows = array();
  foreach ($results as $toon) {
    $row = array();
    $l = sprintf('<img src="http://us.battle.net/wow/static/images/icons/class/%d.gif" title="%s" width="18" height="18" />', $toon->classid, check_plain($toon->class)); 
    $l .= sprintf(' <img src="http://us.battle.net/wow/static/images/icons/race/%d-%d.gif" title="%s" width="18" height="18" /> ', $toon->raceid, $toon->genderid, check_plain($toon->race));
    $l .= l($toon->name, 'toon/' . $toon->tid, array('attributes' => array('class' => array('shadow', 'color-c' . $toon->classid))));
    $row[] = $l;
    $row[] = l($toon->guild_name, 'guild/' . $toon->gid);
    $row[] = $toon->realm;
    $row[] = l($toon->username, 'user/' . $toon->uid);

    $row[] = ($toon->status == 1)?
      sprintf('<img src="/%s/images/validated.png" title="%s" class="validated-img" />', drupal_get_path('module', 'wowtoon'), t('validated character')):
      sprintf('<img src="/%s/images/unvalidated.png" title="%s" class="validated-img" />', drupal_get_path('module', 'wowtoon'), t('unvalidated character'));
    
    
    $actions = array();
    $actions[] = l(t('view'), 'toon/' . $toon->tid);
    $actions[] = l(t('edit'), 'toon/' . $toon->tid . '/edit');
    $row[] = implode(' | ', $actions);
    $rows[] = $row;
  }
  
  $build['table'] = array(
    '#prefix' => '<h3>' . t('WoW Toons') . '</h3>',
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );
  return $build;
}

