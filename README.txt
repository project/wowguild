Welcome to wowguild module.  Wowguild and wowtoon allows a robust guild hosting setup for World of Warcraft
guilds.  Much of the data can be scrapped from [us/eu].battle.net.

*** Dependencies ***

- views.module - Many of the displays leverage views (which allows the administrator to customize views).
- querypath.module - Required to parse data from World of Warcraft armory data.


*** Modules Provided ***

Modules:
wowtoon.module
- Supplies core functionallity allowing users to attach World of Warcraft characters (toons) to their account.
- Allows updates of characters periodically via cron.
- All toons are their own wowtoon entity owned by a user if claimed.
- Each toon's equipment set is saved with it's respective active talent spec (i.e. tank gear is saved when in tanking spec).
- Toons can be 'validated' which will confirm that the user actually owns the toon.  This allows user to
  automatically take care of toon ownership discrepancies.
- Guilds can also be browsed (automatically downloading when viewed), and updated via cron.

wowtoon_account.module
- The user "becomes" their main character in Drupal.
- Posts made by the users will be displayed as posted by their selected toon rather than their username.
- Support for Comments and Forum.

wowguild.module
- Makes the focus of the website a single guild.
- Administrator will select a single guild, and much of the navigation will default to the selected guild.
- Provides a "guild_application" content type to allow authenticated members apply to the guild.
  * Guild Application fields can be customized using standard Drupal fields config.
  * Only one application can be created per toon.
  * Only users with "View Guild Applications" permission can see applications.


*** Installing the Guild Website ***

Place the entirety of this directory in sites/all/modules/wowguild.

1) Enable the Module(s)
 * Navigate to administer >> build >> modules. Enable your selected modules [wowtoon/wowguild].
 * Enable: wowtoon, wowguild, wowtoon_account (optional) and their dependencies: Views, Chaos Tools, QueryPath, Views-UI (optional).
2) Inital Guild Config
 * Add one of your World of Warcraft Characters to your account by clicking on "add character" or navigating to [toon/add].
 * Once you have added your main character, navigate to administer >> configuration >> WoW Guild Settings [admin/config/wowguild/guild].
 * Verify that your guild and realm are correct and click on "Save configuration".
3) Allow users to add characters to their account
 * Navigate to people >> permissions [admin/people/permissions]. And give the desired roles the permission "Add WoW Characters to their own account".

Congratulations, you have a functional Guild hosting site!


*** Recommended Additional Configuration ***
1) Create Additional Roles for Guild Members and/or Guild Admin.
 * Navigate to People >> Permissions >> Roles [admin/people/permissions/roles] and add your desired role name.  e.g. "guild member".
 * Navigate to Permissions [admin/people/permissions] and check and additional permissions the role should have.
2) Tell Drupal not to display "Apply to Guild" to members with "guild member" role.
 * Navigate to administer >> configuration >> WoW Guild Settings [admin/config/wowguild/guild]
 * Add a checkmark to roles that you DO NOT want to see the "apply to guild" links.  e.g. "guild member".
3) Set up cron settings in administer >> configuration >> WoW Toon Settings [admin/config/wowguild/toon].
4) Tell your server to run cron [http://drupal.org/getting-started/6/install/cron]
 * Note: Drupal 7 has added security to run cron.  Replace the cron URL (http://www.example.com/cron.php) with your secure URL found
 under administer >> reports >> status reports [admin/reports/status].  Example (http://www.example.com/cron.php?cron_key=RANDOMCHARACTERS).
  

*** Recommended Modules ***

- Forum Access [http://drupal.org/project/forum_access] - Allow certain forums to be marked as private.
- WYSIWYG [http://drupal.org/project/wysiwyg] or CKEDITOR [http://drupal.org/project/ckeditor] - Allow Rich text editing.
- IMCE [http://drupal.org/project/imce] - Allows users to upload files/images and
- IMCE Wysiwyg API bridge [http://drupal.org/project/imce_wysiwyg] - Adds a button into your selected WYSIWYG editor to upload/insert images.

