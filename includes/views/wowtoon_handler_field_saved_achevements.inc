<?php
// $Id$
/**
 * @file
 * Contains the basic wowtoon achievements field handler.
 */

/**
 * Field handler to provide icons and descriptions for toon/guild achievments.
 */
class wowtoon_handler_field_saved_achievements extends views_handler_field {

  function construct() {
    parent::construct();
    if ($this->definition['field'] == 'description') {
      //$this->additional_fields['classid'] = 'classid';
      //$this->additional_fields['class'] = 'class';
    }
    elseif ($this->definition['field'] == 'icon') {
      //$this->additional_fields['raceid'] = 'raceid';
      //$this->additional_fields['race'] = 'race';
      //$this->additional_fields['genderid'] = 'genderid';
      //$this->additional_fields['gender'] = 'gender';
    }
  }  

  function option_definition() {
    $options = parent::option_definition();
    $options['icon_size'] = array('default' => 18);
    $options['use_interval'] = array('default' => 1);
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    if ($this->definition['field'] == 'icon') {
      $form['icon_size'] = array(
        '#title' => t('Icon size (or url)'),
        '#type' => 'select',
        '#options' => array(0 => 'return url', 18 => 'IMG 18x18', 36 => 'IMG 36x36', 56 => 'IMG 56x56'),
        '#description' => t('Return an IMG tag of size, or just the url if the image.'),
        '#default_value' => isset($this->options['icon_size'])?$this->options['icon_size']:18,
      );
    }
    if ($this->definition['field'] == 'datecompleted') {
      $form['use_interval'] = array(
        '#title' => t('Format date as XX ago style?'),
        '#type' => 'select',
        '#options' => array(0 => 'No, just show date', 1 => 'Yes, use 1 interval (1 day ago)', 2 => 'Yes, use 2 intervals (1 day 2 hours ago)', 3 => 'Yes, use 3 intervals (1 day 2 hours 12 minutes ago)'),
        '#default_value' => isset($this->options['use_interval'])?$this->options['use_interval']:1,
      );
    }
  }
  
  function render($values) {
    if ($this->definition['field'] == 'icon') {
      if ($values->wowtoon_saved_achievements_icon) {
        if ($this->options['icon_size'] == 0) {
          $a = strpos($values->wowtoon_saved_achievements_icon, 'icon/18/') + 8;
          $b = strpos($values->wowtoon_saved_achievements_icon, '"', $a);
          return substr($values->wowtoon_saved_achievements_icon, $a, $b - $a);
        }
        $size = (integer)$this->options['icon_size'];
        $img = str_replace('icons/18', 'icons/' . $size, $values->wowtoon_saved_achievements_icon);
        return sprintf('<img src="%s" width="%d" height="%d" />', $img, $size, $size); 
      }
      return '';
    }
    if ($this->definition['field'] == 'description') {
      return ($values->wowtoon_saved_achievements_description);
    }
    if ($this->definition['field'] == 'datecompleted') {
      if ($this->options['use_interval'] == 0) {
        return format_date($values->wowtoon_saved_achievements_datecompleted);
      }
      else {
        return format_interval(REQUEST_TIME - $values->wowtoon_saved_achievements_datecompleted, $this->options['use_interval']) . ' ' . t('ago');
      }
      
    }
  }
}
