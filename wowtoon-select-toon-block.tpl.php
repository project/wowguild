<?php 
// $Id$

/**
 * @file
 * Renders the block that shows the user's current toon, and allows the user to select from other toons.
 *
 * Available variables:
 * - $name: Current toon's name.
 * - $name_link: Link to the current toon.
 * - $fullname: Current toon's full name (name and title).
 * - $fullname_link: Link to the current toon.
 * - $realm: Current toon's realm.
 * - $level: Current toon's level.
 * - $class: Current toon's class.
 * - $race: Current toon's race.
 * - $guild_name: Current toon's guild name.
 * - $guild_name_link: Link to the toon's guild.
 * 
 * - $image_inset: Inset image of toon.
 * - $faction_class: Sanitized faction used as a class.  (adds alliance or horde blckdrop)
 * - $class_color: Color class for the current toon.
 * - $select_toon_form: HTML form used IF user does not have javascript enabled.
 * - $add_toon_link: Link to add a character to the users account.
 * - $pulldown - HTML 'pulldown' that allows users to change toons.
 * 
 * Other variables:
 * - $toons: Array of toons owned by this user.
 * - $image_card: Card image of the toon.
 * - $image_avatar: Avatar image of the toon.
 * - $current_toon: Current toon's entity.
 *
 * @see template_preprocess_wowtoon_select_toon_block()
 * @see template_preprocess()
 * @see template_process()
 */?>
<div id="select-toon-block-crest" class="<?php echo $faction_class; ?>">
  <!--  block width: 210  inset: 230x116  -->
  <div id="select-toon-block-inset" style="background: url('<?php echo $image_inset; ?>') no-repeat -10px 0; padding-top: 116px;"></div>
  <div id="select-toon-block-current-toon">
    <div id="select-toon-block-name" class="">
    <div id="select-toon-block-char-arrow"></div>
    <?php echo $fullname_link; ?>
    </div>
    <?php
    // This is the javascript select toon pulldown.
    echo $pulldown;
    ?>
    <div id="select-toon-block-class" class="shadow <?php echo $class_color; ?>">
    <span id="select-toon-block-level"><?php echo $level; ?></span>
    <?php echo $race; ?> 
    <?php echo $class; ?>
    </div>
    <div id="select-toon-block-guild"><?php echo $guild_name_link; ?></div>
    <div id="select-toon-block-realm"><?php echo $realm; ?></div>
    <?php echo $select_toon_form; ?>
    <div id="select-toon-block-add-character"> + <?php echo $add_toon_link; ?></div>
  </div>
</div>