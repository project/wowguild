(function ($) {
  /**
   * Copies the character's name into the username field.
   */
  Drupal.Wowtoon.autoAttachUserRegister = function() {
    $("#edit-wowtoon-realm").autocomplete({
	  source: all_realms
    });

    $("#edit-wowtoon-name").change(function() {
    	if($(this).val()) {
    		//if($('#edit-name').val() == '') {
    		  $('#edit-name').val($(this).val());
    		  // Trigger the change event.
    		  $("#edit-name").trigger('change',$(this).val());
    		//}
    	}
    });
  }
  $(Drupal.Wowtoon.autoAttachUserRegister);
})(jQuery);