// $Id$

Welcome to wowguild module.  Wowguild and wowtoon allows a robust guild hosting setup for World of Warcraft
guilds.  Much of the data can be scrapped from [us/eu].blizzard.net.

*** Dependancies ***

 - views.module - Many of the displays leverage views (which allows the administrator to customize views).
 - querypath.module - Required to parse data from World of Warcraft armory data.
 

*** Modules Provided ***

Modules:
wowtoon.module
 - Supplies core functionallity allowing users to attach World of Warcraft characters (toons) to their account.
 - Allows updates of characters periodically via cron.
 - All toons are their own wowtoon entity.
 - Each toon's equipment set is saved with it's respective actice talent spec (i.e. tank gear is saved when in tanking spec).
 - Guilds can also be browsed (automatically downloading when viewed), and updated via cron.
 
wowtoon_account.module
 - The user "becomes" their main character in Drupal.
 - Posts made by the users will be displayed as posted by their selected toon rather than their username.
 - Users can login using their toon's name/realm as well as their username.
 - Support for Comments and Forum.
 
wowguild.module
 - Makes the focus of the website a single guild.
 - Adminstrator will select a single guild, and much of the navigation will default to the selected guild.
 - Provides a guild 'news' feed that appears on the front page.
 
wowtoon_gallery.module


*** Installing as a Guild Website ***

Place the entirety of this directory in sites/all/modules/wowguild.

Navigate to administer >> build >> modules. Enable your selected modules [wowtoon/wowguild].

